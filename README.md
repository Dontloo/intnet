# IntNet
Yet another neural networks library based on Theano.  
Basically a discounted version of Lasagne or Keras, but with improvements w.r.t popular implemetation choices (i.e. my personal needs).

Followings are some improvements to Lasagne at the time of writing.  
1. additional layers: locally shared 2D convolutional layer, contrastive loss layer, unbiased batch normalization  
2. using CuDNN to accelerate pooling and padding  
3. flexible parameter saving/loading  
4. adaptive hyper-parameters  
5. flexible mini-batch loading choices with sampling probabilities  
6. separate constructions of the network and computation graphs (functions)  
7. clearer syntax (to myself at least)  

# features
## network functions
A neural network typically defines two (types of) functions:  
1) loss function *E(theta|x,y)*  
2) output function *f(x|theta)*  

The output function *f(x|theta)* does not have to follow the forward pass of *E*, 
which means, the output network and can be completely different from the training network, 
as long as it uses (a subset of) the trained parameters *theta*.

In short, the training network defines a function of training *theta*, and the output network defines a function based on *theta*.  

## layers
Layers are basic building blocks of networks, a layer defines one particular operation and parameters that may involve. 
**Parameters are declared when instantiating layers, computation graphs are built when instantiating functions, 
such that different functions share the same parameters.** 
Moreover some layers (e.g. dropout, batch normalization) are expected to yield different behaviors at training and test time, 
so in such way we can better decouple the functions from the network.


## adaptive hyper-parameters
Usually we want to change the learning parameters (learning rates, momentum) as training proceeds. 
For learning with multiple losses, we may also want to change our emphasis on different losses during different stages of training. 
So hyper-parameters can be defined as functions of training steps.

## flexible mini-batch sampling
For leveraging imbalanced data sets, we may want to, say draw samples with different probabilities for different categories. 
This is supported as an option in most mini-batch loaders.

# example
```python
# preparing data source
train_ds = intnet.train.CategoricalData(train_lst, "in_memory", x_dtype=floatX)
test_ds = intnet.train.CategoricalData(test_lst, "lazy", x_dtype=floatX)

# input/target layer
l_x = intnet.layer.Input((None, channel, row, column), floatX)
l_y = intnet.layer.Input((None,), "int32")

# network
l_fc = intnet.layer.Activation(intnet.layer.BatchNorm(intnet.layer.FullyConnected(l_x, cate_num)), "softmax")

# loss
l_xent = intnet.layer.CrossEntropy(l_fc, l_y)

# model
foonet = intnet.ffnn.ModelFFNN(l_xent)

# training function
opt = intnet.com.Optimizer("sgd", intnet.com.AdaptParam('fixed', 0.01))
train_fn = foonet.train_fn([l_x, l_y], l_xent, opt)

# output function
l_pred = intnet.layer.ArgMax(l_fc)
pred_fn = foonet.output_fn(l_x, l_pred)

# initialize
foonet.init_param("Glorot_heuristic")

# train
training_steps = 1000
batch_sze = 32
for i, x, y in intnet.train.EpochBatchLauncher(train_ds, batch_sze, training_steps):
    loss = train_fn(x, y)
    print("Iter %d: Loss %g" % (i + 1, loss))

# predict
for i, x, y in intnet.train.EpochBatchLauncher(train_ds, batch_sze):
    print "prediction:", pred_fn(x)
    print "truth:", y

# persistence
print "serializing model parameters."
model_file = "foonet.pickle"
foonet.save_param(model_file)
```

# version log
0.1: basic  
0.2: output/loss/parameter separated  
0.3: batch launcher  
0.4: adaptive parameters  
0.5: parameter persistence   
0.6: network architecture/ computation graph separated  
0.7: batch launcher with sampling probabilities and smoothing, cross validation  
0.8: batch normalization, non-gradient-based updates, separate graphs for training and test

# acknowledgements
I would like to thank IntelliFusion, my employer, for supporting me to work on this awesome project, and that's why the name IntNet.