import timeit
import numpy as np
import theano
import theano.tensor as tt
import theano.typed_list


def numpy_version1(*L):
    n = L[0].shape[1]
    mu = np.zeros((n, n), dtype=L[0].dtype)
    for M in L:
        m_k = np.mean(M, 0)
        tmp = M - m_k
        mu += np.dot(tmp.T, tmp)
        # mu += np.outer(m_k, m_k)
        # res += np.cov(M, rowvar=0 bias=1)
    print mu
    return mu


def lda_fn(i, L, mu, eps):
    m_k = tt.mean(L[i], 0)
    tmp = L[i] - m_k
    return {mu: mu+tt.outer(m_k, m_k), eps: eps+tt.dot(tmp.T, tmp)}


def compile_theano_version2(number_of_matrices, n, val):
    L = theano.typed_list.TypedListType(tt.TensorType(theano.config.floatX, broadcastable=(None, None)))()
    mu = theano.shared(np.zeros((n,n), dtype=theano.config.floatX))
    eps = theano.shared(np.zeros((n,n), dtype=theano.config.floatX))
    result, updates = theano.scan(fn=lda_fn,
                         sequences=[theano.tensor.arange(number_of_matrices, dtype='int64')],
                         non_sequences=[L, mu, eps])
    fn = theano.function([L], updates=updates)
    fn(val)
    # print mu.get_value()
    print eps.get_value()

    # res, _ = theano.scan(fn=lambda i: tt.dot(L[i].T, L[i]),
    #                      sequences=[theano.tensor.arange(number_of_matrices, dtype='int64')])
    # return theano.function([L], res.sum(axis=0))


# def compile_theano_version2(number_of_matrices, n):
#     L = theano.typed_list.TypedListType(tt.TensorType(theano.config.floatX, broadcastable=(None, None)))()
#     res, _ = theano.reduce(fn=lambda i, tmp: tmp+tt.dot(L[i].T, L[i]),
#                            outputs_info=tt.zeros((n, n), dtype=theano.config.floatX),
#                            sequences=[theano.tensor.arange(number_of_matrices, dtype='int64')])
#     return theano.function([L], res)
#     # res, _ = theano.scan(fn=lambda i: tt.dot(L[i].T, L[i]),
#     #                      sequences=[theano.tensor.arange(number_of_matrices, dtype='int64')])
#     # return theano.function([L], res.sum(axis=0))


def compile_theano_version1(number_of_matrices, n):
    L = [tt.matrix(dtype=theano.config.floatX) for _ in xrange(number_of_matrices)]
    res = tt.zeros(n, dtype=theano.config.floatX)
    for M in L:
        res += tt.dot(M.T, M)
    return theano.function(L, res)


def main():
    iteration_count = 1
    number_of_matrices = 10
    n = 160
    min_x = 20
    dtype = 'float32'
    theano.config.floatX = dtype

    L = [np.random.standard_normal(size=(100, n)).astype(dtype)
         for x in range(min_x, number_of_matrices + min_x)]

    # theano_version1 = compile_theano_version1(number_of_matrices, n)
    theano_version2 = compile_theano_version2(number_of_matrices, n, L)


    print "~~~~~~~~~~~compile~~~~~~~~~~~"
    start = timeit.default_timer()
    numpy_res1 = np.sum(numpy_version1(*L)
                        for _ in xrange(iteration_count))
    print 'numpy_version1', timeit.default_timer() - start

    # start = timeit.default_timer()
    # theano_res1 = np.sum(theano_version1(*L)
    #                      for _ in xrange(iteration_count))
    # print 'theano_version1', timeit.default_timer() - start


    # assert np.allclose(numpy_res1, theano_res1, rtol=1e-1)
    # assert np.allclose(theano_res1, theano_res2, rtol=1e-1)


main()
