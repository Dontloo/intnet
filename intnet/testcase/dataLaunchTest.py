import pickle
import intnet
import theano as the
import numpy as np


veri_train_path = "../../../../data/WebFaceNorm/files/mirror1024_100*100_gray.pickle"
with open(veri_train_path, 'rb') as f:
    pickle.load(f)
    veri_train_lst = pickle.load(f)


feature_file_path = "/home/guest/Desktop/dev/git/if_caffe/if/files/jb_features"
feat_dict = {}
with open(feature_file_path) as f:
    break_points = map(int, f.readline().split())
    for line in f:
        tmp = line.split(" ")
        feat_dict[tmp[0]] = np.asarray(map(float, tmp[1:]))


process_data_fn = lambda x:feat_dict[x[len("../../../../data/WebFaceNorm/mirror1024_100*100_gray/"):]]
veri_train_data = intnet.data.CategoricalData(veri_train_lst, "lazy", process_data_fn=process_data_fn, x_dtype=the.config.floatX)

# for x, y in intnet.train.CategoryLauncher(veri_train_data):
#     print x
#     print y
