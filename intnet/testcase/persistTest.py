# Shared variables with float32 dtype are by default moved to the GPU memory space.
# THEANO_FLAGS(mode=FAST_RUN,device=gpu1,floatX=float32,dnn.conv.algo_fwd=time_once,dnn.conv.algo_bwd=time_once,lib.cnmem=0.5
import pickle
import time
import theano as the
import intnet

# the.config.dnn.conv.algo_fwd = "time_once"
# the.config.dnn.conv.algo_bwd = "time_once"

floatX = "float32"
the.config.floatX = floatX

train_lst_path = "../../../../data/YouTubeFaces/files/tmp2_cen160*128_40*32_train.pickle"

print "reading training and test lists."
with open(train_lst_path, 'rb') as f:
    row, column, channel, ppl_num = pickle.load(f)
    train_lst = pickle.load(f)
print row, column, channel, ppl_num

# input/target layer
l_x = intnet.layer.Input((None, channel, row, column), floatX)
l_y = intnet.layer.Input((None, ), "int32")

# network
l_soft = intnet.layer.FullyConnected(l_x, "softmax", ppl_num)
foonet = intnet.ffnn.FFNN(l_soft)

# launch data
x, y = intnet.datapro.launch_all(train_lst, ppl_num, floatX, "one_cold")

# predict
l_pred = intnet.layer.ArgMax(l_soft)
pred_fn = foonet.output_fn(l_x, l_pred)
print pred_fn(x)
print y

foonet.load_param("foonet.pickle")
print pred_fn(x)
print y

# continue training
l_xent = intnet.layer.CrossEntropy(l_soft, l_y)
opt = intnet.com.Optimizer("sgd", 0.00001)
train_fn = foonet.train_fn([l_x, l_y], l_xent, l_xent, opt)
training_steps = 10
for epoch in range(training_steps):
    loss = train_fn(x, y)
    print("Epoch %d: Loss %g" % (epoch + 1, loss))
print pred_fn(x)
print y
