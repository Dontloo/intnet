from intnet.testcase.shapeTest import *

# only current usage covered
# ------------------ test cases ------------------
res = []
# convolution
in_shape = (10, 3, 39, 31)
layer_fn = lambda x: intnet.layer.Conv2d(x, "relu", (5, 5), 20, pad=2)
res.append(single_layer_shape_test(in_shape, layer_fn, "2D convolutional Relu"))
# pooling
in_shape = (10, 3, 25, 25)
layer_fn = lambda x: intnet.layer.Pool2d(x, "max", (2, 2))
res.append(single_layer_shape_test(in_shape, layer_fn, "2D locally pooling max"))
# pooling
in_shape = (10, 3, 6, 6)
layer_fn = lambda x: intnet.layer.Pool2d(x, "average_inc_pad", (6, 6))
res.append(single_layer_shape_test(in_shape, layer_fn, "2D locally pooling average_inc_pad"))
# flattened concatenation
in_shape = [(10, 3, 1, 2), (10, 3, 3, 2)]
layer_fn = intnet.layer.FlatConcat
res.append(single_layer_shape_test(in_shape, layer_fn, "flattened concatenation"))
# cross entropy
in_shape = [(10, 3), (10,)]
layer_fn = lambda x: intnet.layer.CrossEntropy(x[0], x[1])
res.append(single_layer_shape_test(in_shape, layer_fn, "cross entropy"))
# accuracy
in_shape = [(10,), (10,)]
layer_fn = lambda x: intnet.layer.Accuracy(x[0], x[1], "hits")
res.append(single_layer_shape_test(in_shape, layer_fn, "accuracy hits"))
# accuracy
in_shape = [(10,), (10,)]
layer_fn = lambda x: intnet.layer.Accuracy(x[0], x[1], "miss")
res.append(single_layer_shape_test(in_shape, layer_fn, "accuracy miss"))
# accuracy
in_shape = [(10,), (10,)]
layer_fn = lambda x: intnet.layer.Accuracy(x[0], x[1], "acc")
res.append(single_layer_shape_test(in_shape, layer_fn, "accuracy acc"))
# accuracy
in_shape = [(10,), (10,)]
layer_fn = lambda x: intnet.layer.Accuracy(x[0], x[1], "err")
res.append(single_layer_shape_test(in_shape, layer_fn, "accuracy err"))
# argMax
in_shape = (100, 10)
layer_fn = intnet.layer.ArgMax
res.append(single_layer_shape_test(in_shape, layer_fn, "argMax"))
# fully connected
in_shape = (10, 3, 39, 31)
layer_fn = lambda x: intnet.layer.FullyConnected(x, "softmax", 2)
res.append(single_layer_shape_test(in_shape, layer_fn, "fully-connected softmax"))
# fully connected
in_shape = (10, 3, 39, 31)
layer_fn = lambda x: intnet.layer.FullyConnected(x, "relu", 10)
res.append(single_layer_shape_test(in_shape, layer_fn, "fully-connected ReLU"))
# fully connected
in_shape = (10, 3, 39, 31)
layer_fn = lambda x: intnet.layer.FullyConnected(x, "tanh", 10)
res.append(single_layer_shape_test(in_shape, layer_fn, "fully-connected tanh"))
# convolution
in_shape = (10, 3, 39, 31)
layer_fn = lambda x: intnet.layer.Conv2d(x, "softmax", (5, 5), 20)
res.append(single_layer_shape_test(in_shape, layer_fn, "2D convolutional softmax"))
# convolution
in_shape = (10, 3, 39, 31)
layer_fn = lambda x: intnet.layer.Conv2d(x, "relu", (5, 5), 20)
res.append(single_layer_shape_test(in_shape, layer_fn, "2D convolutional Relu"))
# convolution
in_shape = (10, 3, 39, 31)
layer_fn = lambda x: intnet.layer.Conv2d(x, "tanh", (5, 5), 20)
res.append(single_layer_shape_test(in_shape, layer_fn, "2D convolutional tanh"))
# locally convolution
in_shape = (10, 3, 11, 9)
layer_fn = lambda x: intnet.layer.LocalConv2d(x, "relu", (6, 6), 10, (2, 2))
res.append(single_layer_shape_test(in_shape, layer_fn, "2D locally convolutional ReLU"))
# locally convolution
in_shape = (10, 3, 8, 8)
layer_fn = lambda x: intnet.layer.LocalConv2d(x, "tanh", (3, 3), 20, (3, 2))
res.append(single_layer_shape_test(in_shape, layer_fn, "2D locally convolutional ReLU"))

print "Passed: ", res.count(True)
print "Failed: ", res.count(False)
