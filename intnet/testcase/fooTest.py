
import theano as the
import theano.tensor as ten
import theano.tensor.shared_randomstreams
import numpy as np
import timeit
import matplotlib.mlab as ml
import theano.sandbox.cuda.dnn as dnn
import math

from scipy.stats import logistic
import intnet
import pickle
from theano.ifelse import ifelse


# a = ten.TensorType("float64", [False]*2)()
# i = the.shared(np.asarray(range(3)))
# b = a[i,i]
#
# fn = the.function([a], b)
# print fn(np.asarray([[1,2,3],[4,5,6],[7,8,9]]))
#
# a = ten.TensorType("float64", [False]*3)()
# ax = [0,1]
# b = ten.mean(a, axis=ax)
# c = ten.sum(a, axis=ax)/ten.prod(a.shape[ax])
# fn = the.function([a], [b,c])
# print fn(np.asarray([[[1,2,3],[4,5,6],[7,8,9]], [[11,12,13],[14,15,16],[17,18,19]], [[21,22,23],[24,25,26],[27,28,29]]]))

# import itertools
#
# a = [[(1, 0.2), (2, 0.3)], [(3, 0.4), (4, 0.5)]]
# print list(itertools.product(*a))

# a = ten.as_tensor(np.ones((2,2,2,2)))
# fn = the.function([], ten.max(a.flatten(3),axis=2))
# print fn()


a = ten.TensorType("float64", [False])()
b = ten.TensorType("float64", [False])()
z = ten.mean((a-b)**2, axis=-1)
fn = the.function([a, b], z)
print fn([1,2,3], [2,3,4])