import theano
import theano.tensor as T
import numpy as np
import time

# A = T.vector("A")
# idx_range = T.arange(A.shape[0]+1)
# # result is the number of mis-classifications BEFORE each index, thus has n+1 elements.
# result, _ = theano.scan(fn=lambda idx: T.sum(A[:idx])+A.shape[0]-idx-T.sum(A[idx:]), sequences=idx_range)
# result = T.argmin(result)
# count_ones = theano.function(inputs=[A], outputs=result)
#
# print count_ones([0,0,1,0,0,1,1,1])
#
#
in_size = 8
A = T.vector(dtype="float32")
mask1 = theano.shared(np.ones((in_size, in_size))-np.tri(in_size, in_size))
num1 = T.dot(A, mask1)

mask0 = theano.shared(np.tri(in_size, in_size))
idx_vec = theano.shared(1+np.asarray(list(reversed(range(in_size)))))
num0 = idx_vec - T.dot(A, mask0)

num_miss = num0+num1
result = T.argmin(num_miss)
count_ones = theano.function(inputs=[A], outputs=num_miss)
print count_ones(np.asarray([0,0,1,0,0,1,1,1]).astype("float32"))
# [ 4.  3.  2.  3.  2.  1.  2.  3.  4.]

# [4,3,2,2,1,0,0,0]

# iter_num = 1000
# dtype = "float32"
# A = T.vector(dtype=dtype)
# idx_range = T.arange(A.shape[0]+1)
# # result is the number of mis-classifications BEFORE each index, thus has n+1 elements.
# result, _ = theano.scan(fn=lambda idx: T.sum(A[:idx]), sequences=idx_range)
# count_ones = theano.function(inputs=[A], outputs=result)
# start = time.time()
# for _ in range(iter_num):
#     count_ones(np.asarray([0,0,1,0,0,1,1,1]*64).astype(dtype))
# end = time.time()
# print end - start
#
# A = T.vector(dtype=dtype)
# result, updates = theano.scan(fn=lambda prior_result, a: prior_result + a,
#                               outputs_info=T.alloc(np.float32(0), 1),
#                               sequences=A,
#                               n_steps=A.shape[0])
#
# count_ones = theano.function(inputs=[A], outputs=result, updates=updates)
# start = time.time()
# for _ in range(iter_num):
#     count_ones(np.asarray([0,0,1,0,0,1,1,1]*64).astype(dtype))
# end = time.time()
# print end - start
#
# A = T.vector(dtype=dtype)
# in_size = 8*64
# mask = theano.shared(np.tri(in_size))
# result = T.dot(mask, A)
# count_ones = theano.function(inputs=[A], outputs=result)
# start = time.time()
# for _ in range(iter_num):
#     count_ones(np.asarray([0,0,1,0,0,1,1,1]*64).astype(dtype))
# end = time.time()
# print end - start
