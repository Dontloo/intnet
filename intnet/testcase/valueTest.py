import intnet
import theano as the
import numpy as np

l_x = intnet.layer.Input((None, 6), the.config.floatX)
l_out = intnet.layer.Dropout(l_x, 0.5)
net = intnet.ffnn.ModelFFNN(l_out)
fn = net.output_fn([l_x], l_out)
x = np.ones((6,6))
print fn(x)

# # ------------------------------ verification ------------------------------
# l_x = intnet.layer.Input((None, 160), the.config.floatX)
# l_y = intnet.layer.Input((None,), the.config.floatX)
# l_out = intnet.layer.Verification(l_x, l_y, "bounded_hinge", 10)
# test_net = intnet.ffnn.FFNN(l_out)
# out_fn = test_net.output_fn([l_x, l_y], l_out)
# x = np.random.rand(6, 160)
# y = np.asarray([1, 1, 2, 2, 3, 4])
# print out_fn(x, y)

# # ------------------------------ locally connected ------------------------------
# # only current usage covered
# in_shape = (2, 2, 5, 5)
# l_x = intnet.layer.Input((None,) + in_shape[1:], the.config.floatX)
#
# l_out = intnet.layer.LocalConv2d(l_x, "relu", (2, 2), 2, (2, 2))
# print l_out.params["w"].get_value().shape
# w = np.ones(l_out.params["w"].get_value().shape).astype(the.config.floatX)
# # morph the mapping from one input channel to 2222,
# # the output is expected to increase by a half
# w[1, 1, 1, 1, :, :] = np.array([[2, 2], [2, 2]])
# l_out.params["w"].set_value(w)
# test_net = intnet.ffnn.FFNN(l_out)
# out_fn = test_net.output_fn(l_x, l_out)
# x = np.zeros(in_shape).astype(the.config.floatX)
# # initialize using a 5*5 magic matrix
# for i in range(2):
#     for j in range(2):
#         x[i, j, :, :] = np.array([[17, 24, 1, 8, 15],
#                                   [23, 5, 7, 14, 16],
#                                   [4, 6, 13, 20, 22],
#                                   [10, 12, 19, 21, 3],
#                                   [11, 18, 25, 2, 9]])
#
# print out_fn(x)
# # passed
# # [[[[ 138.   74.   60.  106.]
# #   [  76.   62.  108.  144.]
# #   [  64.  100.  146.  132.]
# #   [ 102.  148.  134.   70.]]
# #
# #  [[ 138.   74.   60.  106.]
# #   [  76.   62.  108.  144.]
# #   [  64.  100.  219.  198.]
# #   [ 102.  148.  201.  105.]]]
# #
# #
# # [[[ 138.   74.   60.  106.]
# #   [  76.   62.  108.  144.]
# #   [  64.  100.  146.  132.]
# #   [ 102.  148.  134.   70.]]
# #
# #  [[ 138.   74.   60.  106.]
# #   [  76.   62.  108.  144.]
# #   [  64.  100.  219.  198.]
# #   [ 102.  148.  201.  105.]]]]
