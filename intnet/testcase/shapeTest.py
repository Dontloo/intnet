import numpy
import intnet
import theano as the
import traceback

rng = numpy.random
the.config.floatX = "float32"


def single_layer_shape_test(arg_in_shape, arg_layer_fn, msg):
    """
    test the output shape of a given layer and input shape
    """
    try:
        # initialize
        l_in, x = init_layers_vals(arg_in_shape)
        l_out = arg_layer_fn(l_in)

        net = intnet.ffnn.ModelFFNN(l_out)
        # build function
        out_fn = net.output_fn(l_in, l_out)
        # get output
        # note that we treat parameters x as a list unpacked
        out_shape = out_fn(*x).shape
        # assertion
        print "expected shape", l_out.out_shape
        print "output shape", out_shape
        assert out_shape[1:] == l_out.out_shape[1:]
        print "shape test:", msg, "passed."
        return True
    except:
        traceback.print_exc()
        print "shape test:", msg, "failed."
        return False


def init_layers_vals(arg_in_shape):
    """
    initialize values and layers according to the shape given
    :param arg_in_shape:
    :return:
    """
    if isinstance(arg_in_shape, list):
        in_layers = []
        x = []
        for shape in arg_in_shape:
            # if the shape has only one dimension
            # it is treated as an target variable of int32
            if len(shape) == 1:
                in_layers.append(intnet.layer.Input((None,) + shape[1:], "int32"))
                x.append(rng.randint(size=shape, low=0, high=2).astype("int32"))
            else:
                in_layers.append(intnet.layer.Input((None,) + shape[1:], the.config.floatX))
                x.append(rng.randn(*shape).astype(the.config.floatX))
        return in_layers, x
    else:
        # note that if there is only one input,
        # we wrap it into a list for later convenience
        return intnet.layer.Input((None,) + arg_in_shape[1:], the.config.floatX),\
               [rng.randn(*arg_in_shape).astype(the.config.floatX)]