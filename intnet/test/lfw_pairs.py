import pickle
import intnet
import theano as the
from veri_util import max_acc
from intnet.data.prepro import get_im_pro_fn


def lfw_pair_test(feature_fn, veri_fns):
    pair_lst_file = "../../../../data/LFW/files/pair_webNorm_100*100.pickle"
    # pair_lst_file = "../../../../data/LFW/files/pair_webNorm_37*37.pickle"

    with open(pair_lst_file, 'rb') as f:
        pickle.load(f)
        pair_lst = pickle.load(f)

    pair_ds = intnet.data.PairMatchData(pair_lst, "on_drive", process_data_fn=get_im_pro_fn(), x_dtype=the.config.floatX)

    batch_sze = 300
    f1 = []
    f2 = []
    labs = []
    for i, x1, x2, y in intnet.data.EpochBatchLauncher(pair_ds, batch_sze):
        print x1.shape
        print feature_fn(x1).shape
        f1.extend(feature_fn(x1))
        f2.extend(feature_fn(x2))
        labs.extend(y)

    for veri_fn in veri_fns:
        sims = []
        for i in range(len(f1)):
            sims.append(veri_fn(f1[i], f2[i]))
        max_acc(sims, labs)
