import numpy as np


def max_acc(similarities, labels, ps=3000, ng=3000):
    sim_lst = []
    for i in range(len(labels)):
        sim_lst.append((similarities[i], labels[i]))

    total = float(ps + ng)

    sort_lst = sorted(sim_lst, key=lambda x: x[0], reverse=True)

    ng_before = 0
    ps_after = ps
    acc = 1 - ((ng_before + ps_after) / total)

    sp_point = 1
    max_acc = acc
    for sim, lab in sort_lst:

        if int(lab) == 0:
            ps_after -= 1
        else:
            ng_before += 1
        acc = 1 - ((ng_before + ps_after) / total)

        if acc > max_acc:
            max_acc = acc
            sp_point = sim

    print "MAX Accuracy: %s" % max_acc
    print "Split point:  %s (larger or equal)" % sp_point