import theano as the
import intnet
from base import VeriNet
from intnet.layer import *


class Deepid2(VeriNet):
    def __init__(self, channel, row, column, cate_num, veri, energy_fn, thres, sample, batch_sze, opt):
        """
        initialize the network model and relevant functions
        :param old_model_filename: if given, will continue training on the old model, if not, will initialize a random model
        """
        VeriNet.__init__(self)
        the.config.floatX = "float32"
        print "building net."
        # input/target layer
        l_x = Input((None, channel, row, column), the.config.floatX)
        l_y = Input((None,), "int32")
        # network
        l_conv1 = Activation(BatchNorm(Conv2d(l_x, (4, 4), 20, no_shift=True)), "relu")
        l_pool1 = Pool2d(l_conv1, "max", (2, 2))
        l_conv2 = Activation(BatchNorm(Conv2d(l_pool1, (3, 3), 40, no_shift=True)), "relu")
        l_pool2 = Pool2d(l_conv2, "max", (2, 2))
        l_local3 = Activation(LocalConv2d(l_pool2, (3, 3), 60, (2, 2)), "relu")
        l_pool3 = Pool2d(l_local3, "max", (2, 2))
        l_local4 = Activation(LocalConv2d(l_pool3, (2, 2), 80, (1, 1)), "relu")
        l_concat = Merge([Flatten(l_pool3), Flatten(l_local4)], "concatenate")
        l_deepid = Activation(FullyConnected(l_concat, 160), "relu")
        l_soft = Activation(FullyConnected(l_deepid, cate_num), "softmax")

        # loss/error
        l_xent = CrossEntropy(l_soft, l_y, noise=0.1, cate_num=cate_num)
        if sample.lower() == "triplet":
            l_veri = TripletVerification(l_deepid, l_y, veri, energy_fn, batch_sze, thres)
        elif sample.lower() == "pair":
            l_veri = Verification(l_deepid, l_y, veri, energy_fn, batch_sze, thres)
        else:
            raise ValueError("no sampling methods matched.")

        l_alpha = Input((), the.config.floatX)
        l_loss = Merge([l_xent, l_veri], "lin_comb", [1, l_alpha.in_var])

        self.net = intnet.ffnn.FFNNOptimize(l_loss)

        # deepid layer function
        self.feature_fn = intnet.ffnn.ffnn_fn(l_x, l_deepid)
        self.feature_dim = 160

        # train function
        self.optimizer = intnet.optimize.get_optimizer(*opt)
        self.train_fn = self.net.opt_fn([l_x, l_y, l_alpha], [l_loss, l_xent, l_veri], self.optimizer)

        # validation function
        l_pred = ArgMax(l_soft)
        l_acc = Accuracy(l_pred, l_y, "hits")
        self.test_fn = intnet.ffnn.ffnn_fn([l_x, l_y], [l_acc, l_pred])

        self.sampler = sample
