# Shared variables with float32 dtype are by default moved to the GPU memory space.
# THEANO_FLAGS(mode=FAST_RUN,device=gpu1,floatX=float32,dnn.conv.algo_bwd_filter=time_once,dnn.conv.algo_bwd=time_once,lib.cnmem=0.5
import pickle
import theano as the
import intnet
import intnet.layer as lay
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt

# the.config.dnn.conv.algo_fwd = "time_once"
# the.config.dnn.conv.algo_bwd = "time_once"

floatX = "float32"
the.config.floatX = floatX

train_file_path = "/home/guest/Desktop/dev/data/mnist/files/train.txt"
test_file_path = "/home/guest/Desktop/dev/data/mnist/files/test.txt"

print "reading training and test lists."
train_lst = intnet.data.read_caffe_lst(train_file_path)
test_lst = intnet.data.read_caffe_lst(test_file_path)

row, column, channel, cate_num = 16, 16, 1, 10
batch_sze = 100

# preparing data
im_pro_fn = intnet.data.get_im_pro_fn(color_space="L", scale=True, flatten=True, resize_interval=(4/7., 4/7.))  # load image as grayscale
train_ds = intnet.data.CategoricalData(train_lst, "lazy", process_data_fn=im_pro_fn, x_dtype=floatX)
test_ds = intnet.data.CategoricalData(test_lst, "lazy", process_data_fn=im_pro_fn, x_dtype=floatX)

# encoder
l_x = lay.Input((batch_sze, row*column), floatX)
l_h = lay.Activation(lay.FullyConnected(l_x, 256), "relu")
l_z_mean = lay.FullyConnected(l_h, 2)
l_z_log_var = lay.FullyConnected(l_h, 2)
# sampling
l_z = lay.GaussSample(l_z_mean, l_z_log_var)
# decoder
l_h_dec = lay.Activation(lay.FullyConnected(l_z, 256), "relu")
l_x_dec = lay.Activation(lay.FullyConnected(l_h_dec, row*column), "sigmoid")

# loss
l_xent = lay.BinaryXent(l_x_dec, l_x)
l_kl = lay.KL(l_z_mean, l_z_log_var)
l_loss = lay.Merge([l_xent, l_kl], "sum")

# an ffnn is essentially a function to be optimized, defined by the loss and related layers
net = intnet.ffnn.FFNNOptimize(l_loss)

# initialize
net.init_param("Glorot_heuristic")

# an optimizer instance has its own (shared) parameters that need to be updated every iteration
lr = intnet.com.AdaptParam('linear', 0.002, 0.0002)
momentum = intnet.com.AdaptParam('fixed', 0.9)
opt = intnet.optimize.get_optimizer("adam", lr, momentum)
# output loss, parameter and gradient
train_fn = net.opt_fn([l_x], [l_loss, l_xent, l_kl], opt)

# # train
# train_steps = 3e4
# for i, x, y in intnet.data.EpochBatchLauncher(train_ds, batch_sze, train_steps, shuffle=True):
#     opt.update_learning_params(i, train_steps)
#     loss, xent, kl = train_fn(x)
#     if (i + 1)%100 == 0:
#         print("Iter %d: Loss %g" % (i + 1, loss)), xent, kl
#
# # persistence
# print "serializing model parameters."
# model_file = "sae.pickle"
# net.save_param(model_file)

# read parameter
net.init_param("sae.pickle")

# decode
l_z_in = lay.Input((None, 2), floatX)
l_h_dec.pred.pred = l_z_in
decoder_fn = intnet.ffnn.ffnn_fn(l_z_in, l_x_dec)
x_gen = decoder_fn(np.asarray(
    [[3.86136234e-01,  6.77061677e-01],
     [ 0.00952704, -0.06975581],
     [-2,  -2]], dtype=floatX))
print x_gen
plt.imshow(x_gen[2].reshape(row, column), cmap='Greys_r')
plt.show()
