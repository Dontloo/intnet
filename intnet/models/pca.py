import matplotlib.mlab as ml
import intnet
import numpy as np


class PCA:
    def __init__(self, data, dim, standardize=False):
        self.pca = ml.PCA(data, standardize=standardize)
        self.dim = dim

    def project(self, x):
        '''
        p1 = self.project(features)
        p2 = self.pca.Y[:, :200]
        np.allclose(p1,p2, atol=1e-6)  # true
        '''
        x = np.asarray(x)

        ndims = len(x.shape)

        if (x.shape[-1] != self.pca.numcols):
            raise ValueError('Expected an array with dims[-1]==%d' %
                             self.pca.numcols)

        Yreduced = np.dot(self.pca.Wt[:self.dim], self.pca.center(x).T).T
        return Yreduced