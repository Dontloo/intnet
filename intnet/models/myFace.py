import time
import intnet
import theano as the
from base import VeriNet
from intnet.layer import *


class MyFace(VeriNet):
    def __init__(self, channel, row, column, cate_num, lab_noise, veri, energy_fn, thres, sample, batch_sze, opt):
        """
        initialize the network model and relevant functions
        :param old_model_filename: if given, will continue training on the old model, if not, will initialize a random model
        """
        VeriNet.__init__(self)
        the.config.floatX = "float32"
        print "building net."
        # input/target layer
        l_x = Input((None, channel, row, column), the.config.floatX)
        l_y = Input((None,), "int32")
        # network
        l_conv11 = Activation(BatchNorm(Conv2d(l_x, (3, 3), 32, name='Conv11', no_shift=True)), "relu")  # 98
        l_conv12 = Activation(BatchNorm(Conv2d(l_conv11, (3, 3), 64, name='Conv12', no_shift=True)), "relu")  # 96
        l_pool1 = Pool2d(l_conv12, "max", (2, 2))  # 48
        l_conv21 = Activation(BatchNorm(Conv2d(l_pool1, (3, 3), 64, pad=1, name='Conv21', no_shift=True)), "relu")  # 48
        l_conv22 = Activation(BatchNorm(Conv2d(l_conv21, (3, 3), 128, pad=1, name='Conv22', no_shift=True)), "relu")  # 48
        l_pool2 = Pool2d(l_conv22, "max", (2, 2))  # 24
        l_conv31 = Activation(BatchNorm(Conv2d(l_pool2, (3, 3), 96, pad=1, name='Conv31', no_shift=True)), "relu")  # 24
        l_conv32 = Activation(BatchNorm(Conv2d(l_conv31, (3, 3), 192, pad=1, name='Conv32', no_shift=True)), "relu")  # 24
        l_pool3 = Pool2d(l_conv32, "max", (2, 2))  # 12
        l_conv41 = Activation(BatchNorm(Conv2d(l_pool3, (3, 3), 128, pad=1, name='Conv41', no_shift=True)), "relu")  # 12
        l_conv42 = Activation(BatchNorm(Conv2d(l_conv41, (3, 3), 256, pad=1, name='Conv42', no_shift=True)), "relu")  # 12
        l_pool4 = Pool2d(l_conv42, "max", (2, 2))  # 6
        l_conv51 = Activation(BatchNorm(Conv2d(l_pool4, (3, 3), 160, pad=1, name='Conv51', no_shift=True)), "relu")  # 6
        l_conv52 = Activation(BatchNorm(Conv2d(l_conv51, (3, 3), 320, pad=1, name='Conv52', no_shift=True)), "relu")  # 6

        l_res = ResBottle(l_conv52, 240, 320, "Res")

        l_pool5 = Pool2d(l_res, "average", "global")  # 1
        l_drop = Dropout(l_pool5, 0.4)  # 320
        l_fc = FullyConnected(l_drop, cate_num, name='FC6_'+str(cate_num))
        l_soft = Activation(l_fc, "softmax")

        # # loss/error
        # l_xent = CrossEntropy(l_soft, l_y, noise=lab_noise, cate_num=cate_num)
        # if sample.lower() == "triplet":
        #     l_veri = TripletVerification(l_pool5, l_y, veri, energy_fn, batch_sze, thres, margin=0.3)
        # elif sample.lower() == "pair":
        #     l_veri = Verification(l_pool5, l_y, veri, energy_fn, batch_sze, thres, margin=0.3)
        # else:
        #     raise ValueError("no sampling methods matched.")
        #
        l_alpha = Input((), the.config.floatX)
        # # l_regular = Regular([l_fc], "l2")
        # # l_loss = Merge([l_xent, l_veri, l_regular], "lin_comb", [1, l_alpha.in_var, 2e-5])
        # l_regular = Regular([l_fc], "l2")
        # l_loss = Merge([l_xent, l_veri], "lin_comb", [1, l_alpha.in_var])
        l_loss = CrossEntropy(l_soft, l_y, noise=lab_noise, cate_num=cate_num)

        # train function
        self.net = intnet.ffnn.FFNNOptimize(l_loss)
        self.optimizer = opt
        self.train_fn = self.net.opt_fn([l_x, l_y, l_alpha], [l_loss], self.optimizer)

        # validation function
        l_pred = ArgMax(l_soft)
        l_acc = Accuracy(l_pred, l_y, "hits")
        self.test_fn = intnet.ffnn.ffnn_fn([l_x, l_y], [l_acc, l_pred])

        # feature function
        self.feature_fn = intnet.ffnn.ffnn_fn(l_x, l_pool5)
        self.feature_dim = 320

        self.sampler = sample
