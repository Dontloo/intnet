# THEANO_FLAGS='mode=FAST_RUN,device=gpu1,floatX=float32,dnn.conv.algo_fwd=time_once,dnn.conv.algo_bwd_filter=time_once,lib.cnmem=0.8'
# PYTHONPATH=/home/guest/Desktop/dev/git/intnet python deepidToy.py
# Shared variables with float32 dtype are by default moved to the GPU memory space.

import pickle
import intnet
from deepid1 import Deepid1
from deepid2 import Deepid2
from webFace import WebFace
from myFace import MyFace
from pca import PCA
from intnet.jointBayesian import JointBayesian
import numpy as np
from intnet.com import AdaptParam
import theano as the
import matplotlib.mlab as ml
import time
import intnet.test


print "reading training and test lists."
# ------------------------- split training -------------------------
file_lst_path = "/home/guest/Desktop/dev/data/MsCeleb/files/mixed_35837_100*100_tr0.6.txt"
train_lst = intnet.data.read_caffe_lst(file_lst_path)

row, column, channel, cate_num = 84, 84, 3, 35837
im_pro_fn = intnet.data.get_im_pro_fn(color_space="RGB", resize_interval=(0.91, 1), crop_shape=(row, column))  # load image as grayscale

# # --------------------------------------------------
# file_path = "../../../../data/WebFaceNorm/files/ac1024_37*37_gray.pickle"
#
# with open(file_path, 'rb') as f:
#     row, column, channel, cate_num = pickle.load(f)
#     data_lst = pickle.load(f)
#
# train_lst, vali_lst = intnet.data.train_test_split_per_cate(data_lst, cate_num, arg_cut=0.9)


# # ------------------------- model -------------------------
# train_storage = "in_memory"
train_storage = "lazy"
test_storage = "lazy"
ds_pro = 1
lab_noise = 0

# parameters
train_steps = 3e5
batch_sze = 48

# adapt_lr = AdaptParam("linear", 1e-2, 1e-4)
adapt_lr = AdaptParam("step", [(2e-3, 0.4), (2e-4, 0.8), (2e-5, 1)])
# adapt_lr = AdaptParam("fixed", 1e-3)
# adapt_m = AdaptParam("linear", 0.5, 0.9)
adapt_m = AdaptParam("fixed", 0.9)
opt = intnet.optimize.get_optimizer("adam", adapt_lr, adapt_m)

# results
# old_model = None
old_model = "deep2.8.pickle"
new_model = "deep2.8.pickle"
plot_file = "curves0.png"

train_ds = intnet.data.CategoricalData(train_lst, train_storage, process_data_fn=im_pro_fn, x_dtype=the.config.floatX)
# test_ds = intnet.data.CategoricalData(vali_lst, test_storage, process_data_fn=im_pro_fn, x_dtype=the.config.floatX)

# # ------------------------- deep1 -------------------------
# deep_net = Deepid1(channel, row, column, cate_num, old_model)
# deep_net.train(train_lst, train_storage, new_model, plot_file, train_steps, batch_sze, l_hi, l_lo, m_ini, m_inc)
# deep_net.vali(vali_lst, test_storage, batch_sze)

# # ------------------------- verification -------------------------
# sample = "pair"
sample = "triplet"
# adapt_alpha = AdaptParam("linear", 6e-5, 1e-3)
adapt_alpha = AdaptParam("fixed", 0)
# veri = "cluster"
# energy_fn = "euc_sqr"
veri = "spring"
energy_fn = "euc"
thres = None

# # ------------------------- deep2 -------------------------
# deep_net = Deepid2(channel, row, column, cate_num, veri, energy_fn, thres, sample, batch_sze, opt)
# deep_net.init_param(old_model)
# deep_net.train(train_ds, new_model, plot_file, train_steps, batch_sze, adapt_alpha, ds_pro)
# deep_net.test(test_ds, batch_sze)

# # ------------------------- webface -------------------------
# for i in range(4):
#     s = time.time()
#     deep_net = WebFace(channel, row, column, cate_num, old_model, veri, energy_fn, thres, sample, batch_sze, opt)
#     # deep_net.train(train_lst, train_storage, new_model, plot_file, train_steps, batch_sze, adapt_alpha)
#     deep_net.vali(vali_lst, test_storage, batch_sze)
#     print "time elapsed:", time.time() - s

# ------------------------- myface -------------------------
deep_net = MyFace(channel, row, column, cate_num, lab_noise, veri, energy_fn, thres, sample, batch_sze, opt)
deep_net.init_param(old_model)
deep_net.train(train_ds, new_model, plot_file, train_steps, batch_sze, adapt_alpha, ds_pro)
deep_net.test(train_ds, batch_sze)

# # ------------------------- verification & joint Bayesian-------------------------

import intnet.test
feature_dim = deep_net.feature_dim
feature_fn = deep_net.feature_fn

# feature_dim = 300
# pca = PCA(features, feature_dim, standardize=False)
# feature_fn = lambda x: pca.project(deep_net.feature_fn(x))

veri_fns = [lambda x, y:-(sum(x**2)+sum(y**2)-2*sum(x*y)), lambda x, y: -(sum(x**2)+sum(y**2)-3*sum(x*y)), lambda x, y: -(sum(x**2)+sum(y**2)-4*sum(x*y))]
intnet.test.lfw_pair_test(feature_fn, veri_fns)
