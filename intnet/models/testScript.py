# THEANO_FLAGS='mode=FAST_RUN,device=gpu1,floatX=float32,dnn.conv.algo_fwd=time_once,dnn.conv.algo_bwd_filter=time_once,dnn.conv.algo_bwd_data=time_once,lib.cnmem=0.8'
# PYTHONPATH=/home/guest/Desktop/dev/git/intnet python deepidToy.py
# Shared variables with float32 dtype are by default moved to the GPU memory space.

import pickle
import intnet
from deepid1 import Deepid1
from deepid2 import Deepid2
from webFace import WebFace
from resNet import ResNet
from myFace import MyFace
from pca import PCA
from intnet.jointBayesian import JointBayesian
import numpy as np
from intnet.com import AdaptParam
import theano as the
import sys
import matplotlib.mlab as ml


def para_test(adapt_alpha_lo, adapt_alpha_hi, stgy='batch', ds_pro=1, sample='pair', train_steps=1e5, reg_w=1e-4):
    print "reading training and test lists."
    # ------------------------- split training -------------------------
    # train_path = "../../../../data/WebFaceNorm/files/ac1024_37*37_gray_0.8.pickle"
    # vali_path = "../../../../data/WebFaceNorm/files/ac1024_37*37_gray_0.2.pickle"

    # train_path = "../../../../data/WebFaceNorm/files/ac1024_100*100_gray_0.8.pickle"
    # vali_path = "../../../../data/WebFaceNorm/files/ac1024_100*100_gray_0.2.pickle"
    train_path = "../../../../data/WebFaceNorm/files/mirror10575_100*100_gray.pickle"
    # vali_path = "../../../../data/WebFaceNorm/files/mirror10575_100*100_gray.pickle"

    with open(train_path, 'rb') as f:
        row, column, channel, cate_num = pickle.load(f)
        train_lst = pickle.load(f)
        print row, column, channel, cate_num

    # try:
    #     with open(vali_path, 'rb') as f:
    #         pickle.load(f)
    #         vali_lst = pickle.load(f)
    # except NameError:
    #     vali_lst = train_lst

    # --------------------------------------------------
    # file_path = "../../../../data/WebFaceNorm/files/mirror10575_37*37_gray.pickle"
    # # file_path = "../../../../data/WebFace/color100sdk/files/ac10575_100*100_RGB.pickle"
    #
    #
    # with open(file_path, 'rb') as f:
    #     row, column, channel, cate_num = pickle.load(f)
    #     data_lst = pickle.load(f)
    #
    # train_lst, vali_lst = intnet.train.train_test_split_per_cate(data_lst, cate_num, arg_cut=0.9)

    # # ------------------------- model -------------------------
    # train_storage = "in_memory"
    train_storage = "lazy"
    test_storage = "lazy"

    # parameters
    train_steps = train_steps
    batch_sze = 48

    # adapt_lr = AdaptParam("linear", 2e-3, 1e-6)
    adapt_lr = AdaptParam("step", [(0.75, 1e-3), (0.95, 1e-4), (1, 1e-5)])
    # adapt_lr = AdaptParam("fixed", 1e-2)

    # adapt_m = AdaptParam("linear", 0.5, 0.9)
    adapt_m = AdaptParam("fixed", 0.9)

    # opt = ("my_ada", adapt_lr, adapt_m)
    opt = ("adam", adapt_lr, adapt_m)
    # opt = ("nesterov", adapt_lr, adapt_m)

    # results
    old_model = None
    # old_model = "deep2.8.pickle"
    # old_model = "webfaceBN.1.pickle"
    # old_model = "myface.0.pickle"

    # new_model = "webfaceBN.3.pickle"
    # new_model = "myface.2.pickle"
    new_model = "deep2.8.pickle"
    plot_file = "curves0.png"
    # # ------------------------- verification -------------------------

    # veri = "hinge"
    # energy_fn = "euc_sqr"
    veri = "spring"
    energy_fn = "euc"

    # thres = 1
    thres = None

    # sample = "triplet"
    # sample = "pair"

    print adapt_alpha_lo, adapt_alpha_hi
    adapt_alpha = AdaptParam("linear", adapt_alpha_lo, adapt_alpha_hi)
    # adapt_alpha = AdaptParam("fixed", 0)

    # ------------------------- train -------------------------
    train_acc = []
    vali_acc = []

    # deep_net = WebFace(channel, row, column, cate_num, old_model, veri, energy_fn, thres, sample, batch_sze, opt)
    deep_net = MyFace(channel, row, column, cate_num, old_model, veri, energy_fn, thres, sample, batch_sze, opt, reg_w)
    # deep_net = ResNet(channel, row, column, cate_num, old_model, veri, energy_fn, thres, sample, batch_sze, opt)
    # deep_net = Deepid2(channel, row, column, cate_num, veri, energy_fn, thres, sample, batch_sze, opt)
    # deep_net = MyDeepid(channel, row, column, cate_num, veri, energy_fn, thres, sample, batch_sze, opt)

    train_ds = intnet.data.CategoricalData(train_lst, train_storage, x_dtype=the.config.floatX)
    # vali_ds = intnet.train.CategoricalData(vali_lst, test_storage, x_dtype=the.config.floatX)
    print train_ds.size

    num_trains = 1
    for i in range(num_trains):
        print "*** num_trains:", i, " ***"
        deep_net.init_param(old_model)
        deep_net.train(train_ds, new_model, plot_file, train_steps, batch_sze, adapt_alpha, stgy=stgy, ds_pro=ds_pro)
        # deep_net.test(vali_ds, batch_sze)
        train_acc.append(deep_net.train_acc)
        # vali_acc.append(deep_net.vali_acc)
        print "*** num_trains: %s, train_acc: %s, vali_acc: %s ***" % (i, train_acc, vali_acc)

    avg_train = sum(train_acc) / len(train_acc)
    avg_vali = None
    try:
        avg_vali = sum(vali_acc) / len(vali_acc)
    except:
        pass
    print "train acc", train_acc, "max:", max(train_acc), "avg:", avg_train
    #  "vali acc", vali_acc, "max:", max(vali_acc), "avg:", avg_vali

    # # ------------------------- test -------------------------
    # deep_net = WebFace(channel, row, column, cate_num, old_model, veri, energy_fn, thres, sample, batch_sze, opt)
    # deep_net.train(train_lst, train_storage, new_model, plot_file, train_steps, batch_sze, adapt_alpha)
    # deep_net.vali(vali_lst, test_storage, batch_sze)

    # # ------------------------- verification & joint Bayesian-------------------------
    pair_lst_file = "../../../../data/LFW/files/pair_webNorm_100*100.pickle"
    veri_train_path = "../../../../data/WebFaceNorm/files/mirror1024_100*100_gray.pickle"
    # veri_train_path = "../../../../data/WebFaceNorm/files/ac1024_100*100_gray_0.8.pickle"
    #
    # pair_lst_file = "../../../../data/LFW/files/pair_webNorm_37*37.pickle"
    # veri_train_path = "../../../../data/WebFaceNorm/files/ac1024_37*37_gray.pickle"
    # veri_train_path = "../../../../data/WebFaceNorm/files/mirror10575_37*37_gray.pickle"
    # pair_lst_file = "../../../../data/LFW/files/pair_color100sdk_100*100_RGB.pickle"
    # veri_train_path = "../../../../data/WebFace/color100sdk/files/ac1024_100*100_RGB.pickle"

    with open(pair_lst_file, 'rb') as f:
        pickle.load(f)
        pair_lst = pickle.load(f)

    with open(veri_train_path, 'rb') as f:
        pickle.load(f)
        veri_train_lst = pickle.load(f)

    veri_train_data = intnet.data.CategoricalData(veri_train_lst, "lazy", x_dtype=the.config.floatX)

    features, _ = intnet.datapro.get_feature(veri_train_data, deep_net.feature_fn)

    feature_dim = deep_net.feature_dim
    feature_fn = deep_net.feature_fn

    # feature_dim = 300
    # pca = PCA(features, feature_dim, standardize=False)
    # feature_fn = lambda x: pca.project(deep_net.feature_fn(x))

    jb = JointBayesian(feature_fn, feature_dim, np.mean(features, 0))  # np.mean(features, 0)
    acc1 = jb.veri(pair_lst)
    jb.joint_bayesian_EM(veri_train_data, cov_ini="lda", iter=0, thres=1e-4, inv_fn=np.linalg.pinv)
    acc2 = jb.veri(pair_lst)
    jb.joint_bayesian_EM(veri_train_data, cov_ini="eye", iter=50, thres=1e-4, inv_fn=np.linalg.pinv)
    acc3 = jb.veri(pair_lst)

    print "verification acc: ", acc1, acc2, acc3
    print "max", max(acc1, acc2, acc3)
    return avg_train, avg_vali, acc1, acc2, acc3

# print(para_test(0.001, 0.1))
