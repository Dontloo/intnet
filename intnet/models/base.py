import time
import theano as the
import intnet
import numpy as np
import datetime


class VeriNet:
    def __init__(self):
        self.net = None
        self.optimizer = None
        self.sampler = None
        self.train_fn = None
        self.test_fn = None
        pass

    def init_param(self, old_model_filename):
        print "initializing parameters."
        self.net.init_param(old_model_filename)
        # initialize optimizer
        self.optimizer.set_optimizer_param("reset")

    def train(self, train_ds, new_model_filename, plot_file, train_steps, batch_sze, adapt_alpha, ds_pro):
        """
        train a deepid network
        """
        # train
        s = time.time()
        # launcher
        if self.sampler.lower() == "triplet":
            train_launcher = intnet.data.RandomTripletLauncher
        elif self.sampler.lower() == "pair":
            train_launcher = intnet.data.RandomPairLauncher
        else:
            raise ValueError("no sampling methods matched.")
        for i, x, y in train_launcher(train_ds, batch_sze, train_steps, ds_pro=ds_pro):
            # update learning parameters
            self.optimizer.update_learning_params(i, train_steps)
            # change alpha gradually
            res = self.train_fn(x, y, adapt_alpha.get_val(i, train_steps))
            if (i + 1) % 100 == 0:
                print datetime.datetime.now(), "Iter %d: Loss %g =" % (i + 1, res[0]), \
                    " + ".join(map(lambda v: str(np.asarray(v)), res[1:])), adapt_alpha.get_val(i, train_steps)

        e = time.time()
        print "time elapsed:", e - s

        # training error
        total_hits = 0
        for i, x, y in intnet.data.EpochBatchLauncher(train_ds, batch_sze):
            hits, _ = self.test_fn(x, y)
            total_hits += hits
        print "training acc:", "hits:", total_hits, "/", train_ds.size, float(total_hits) / train_ds.size
        self.train_acc = float(total_hits) / train_ds.size

        # persistence
        print "serializing training and test lists."
        self.net.save_param(new_model_filename)
        self.net.plot(['loss', 'xent', 'veri'], plot_file)

    def test(self, test_ds, batch_sze):
        # predict
        total_hits = 0
        for i, x, y in intnet.data.EpochBatchLauncher(test_ds, batch_sze):
            hits, predict = self.test_fn(x, y)
            total_hits += hits
        print "test acc:", "hits:", total_hits, "/", test_ds.size, float(total_hits) / test_ds.size
        self.vali_acc = float(total_hits) / test_ds.size
