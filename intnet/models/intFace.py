import time
import intnet
import theano as the


class IntFace:
    def __init__(self, channel, row, column, cate_num, old_model_filename, veri, energy_fn, thres, sample, batch_sze, opt):
        """
        initialize the network model and relevant functions
        :param old_model_filename: if given, will continue training on the old model, if not, will initialize a random model
        """
        the.config.floatX = "float32"
        print "building net."
        # input/target layer
        l_x = intnet.layer.Input((None, channel, row, column), the.config.floatX)
        l_y = intnet.layer.Input((None,), "int32")
        # network
        l_conv11 = intnet.layer.Conv2d(l_x, "relu", (3, 3), 32, name='Cov11')  # 98
        l_conv12 = intnet.layer.Conv2d(l_conv11, "relu", (3, 3), 64, name='Cov12')  # 96
        l_pool1 = intnet.layer.Pool2d(l_conv12, "max", (2, 2))  # 48
        l_conv21 = intnet.layer.Conv2d(l_pool1, "relu", (3, 3), 64, pad=1, name='Cov21')  # 48
        l_conv22 = intnet.layer.Conv2d(l_conv21, "relu", (3, 3), 128, pad=1, name='Cov22')  # 48
        l_pool2 = intnet.layer.Pool2d(l_conv22, "max", (2, 2))  # 24
        l_conv31 = intnet.layer.Conv2d(l_pool2, "relu", (3, 3), 96, pad=1, name='Cov31')  # 24
        l_conv32 = intnet.layer.Conv2d(l_conv31, "relu", (3, 3), 192, pad=1, name='Cov32')  # 24
        l_pool3 = intnet.layer.Pool2d(l_conv32, "max", (2, 2))  # 12
        l_conv31 = intnet.layer.Conv2d(l_pool3, "relu", (3, 3), 128, pad=1, name='Cov41')  # 12
        l_conv32 = intnet.layer.Conv2d(l_conv31, "relu", (3, 3), 256, pad=1, name='Cov42')  # 12
        l_pool4 = intnet.layer.Pool2d(l_conv32, "max", (2, 2))  # 6
        l_conv41 = intnet.layer.Conv2d(l_pool4, "relu", (3, 3), 160, pad=1, name='Cov51')  # 6
        l_conv42 = intnet.layer.Conv2d(l_conv41, "relu", (3, 3), 320, pad=1, name='Cov52')  # 6
        l_pool5 = intnet.layer.Pool2d(l_conv42, "average_inc_pad", (6, 6))  # 1
        l_flat = intnet.layer.Flatten(l_pool5)  # 320
        l_drop = intnet.layer.Dropout(l_flat, 0.4)
        l_soft = intnet.layer.FullyConnected(l_drop, "softmax", cate_num, name='FC6')

        # loss/error
        l_xent = intnet.layer.CrossEntropy(l_soft, l_y)
        l_veri = intnet.layer.Verification(l_flat, l_y, veri, energy_fn, thres, batch_sze)  # thres=5 pair_num=batch_sze/2
        l_alpha = intnet.layer.Input((), the.config.floatX)
        l_loss = intnet.layer.LinearComb([l_xent, l_veri], [1, l_alpha.in_var])

        # train function
        self.webface_net = intnet.ffnn.ModelFFNN(l_loss)
        self.optimizer = intnet.com.Optimizer(*opt)
        self.train_fn = self.webface_net.train_fn([l_x, l_y, l_alpha], [l_xent, l_veri, l_loss], self.optimizer)

        # validation function
        l_soft.pred = l_pool5  # skip dropout layer
        l_pred = intnet.layer.ArgMax(l_soft)
        l_acc = intnet.layer.Accuracy(l_pred, l_y, "hits")
        self.valid_fn = intnet.ffnn.output_fn([l_x, l_y], [l_pred, l_acc])

        # feature function
        self.feature_fn = intnet.ffnn.output_fn(l_x, l_flat)
        self.feature_dim = 320

        # load/init model
        self.webface_net.init_param(old_model_filename)

    def train(self, train_lst, train_storage, new_model_filename, plot_file, train_steps, batch_sze, adapt_alpha):
        """
        train a deepid network
        """
        print "loading training data."
        train_dat = intnet.data.CategoricalData(train_lst, train_storage, x_dtype=the.config.floatX)

        s = time.time()
        for i, x, y in intnet.data.RandomPairLauncher(train_steps, batch_sze, train_dat):
            # update learning parameters
            self.optimizer.update_learning_params(i, train_steps)
            # change alpha gradually
            xent, veri, loss = self.train_fn(x, y, adapt_alpha.get_val(i, train_steps))
            print "Iter %d: Loss %g = %g + %g" % (i + 1, loss, xent, veri)
        e = time.time()
        print "time elapsed:", e - s

        # training error
        total_hits = 0
        for i, x, y in intnet.data.SplitBatchLauncher(batch_sze, train_dat):
            _, hits = self.valid_fn(x, y)
            total_hits += hits
        print "training acc:", "hits:", total_hits, "/", train_dat.size, float(total_hits) / train_dat.size

        # persistence
        print "serializing training and test lists."
        self.webface_net.save_param(new_model_filename)
        self.webface_net.plot(['xent', 'veri', 'loss'], plot_file)

    def vali(self, vali_lst, vali_storage, batch_sze):
        # predict
        test_dat = intnet.data.CategoricalData(vali_lst, vali_storage, x_dtype=the.config.floatX)
        total_hits = 0
        for i, x, y in intnet.data.SplitBatchLauncher(batch_sze, test_dat):
            predict, hits = self.valid_fn(x, y)
            total_hits += hits
        print "test acc:", "hits:", total_hits, "/", test_dat.size, float(total_hits) / test_dat.size


if __name__ == "__main__":
    # THEANO_FLAGS='mode=FAST_RUN,device=gpu1,floatX=float32,dnn.conv.algo_fwd=time_once,dnn.conv.algo_bwd=time_once,lib.cnmem=0.8'
    # PYTHONPATH=/home/guest/Desktop/dev/git/intnet python deepidToy.py
    # Shared variables with float32 dtype are by default moved to the GPU memory space.
    import pickle
    import intnet
    from intnet.com import AdaptParam
    from intnet.jointBayesian import JointBayesian
    import numpy as np

    # # ------------------------- split training -------------------------
    train_path = "../../../../data/WebFaceNorm/files/mirror10575_100*100_gray_0.9.pickle"
    vali_path = "../../../../data/WebFaceNorm/files/mirror10575_100*100_gray_0.1.pickle"

    print "reading training and test lists."
    with open(train_path, 'rb') as f:
        row, column, channel, cate_num = pickle.load(f)
        train_lst = pickle.load(f)
    with open(vali_path, 'rb') as f:
        pickle.load(f)
        vali_lst = pickle.load(f)
    # # ------------------------- split training -------------------------

    # train_storage = "lazy"
    train_storage = "in_memory"
    test_storage = "lazy"

    # results
    old_model = None
    new_model = "intface.0.pickle"
    plot_file = "curves1.png"

    # parameters
    train_steps = 6e5
    batch_sze = 32

    adapt_lr = AdaptParam("linear", 1e-3, 1e-5)
    # adapt_lr = AdaptParam("step", [(0.8, 1e-3), (1, 1e-4)])
    adapt_m = AdaptParam("linear", 0.9, 0.9)
    opt = ("my_ada", adapt_lr, adapt_m)

    # # ------------------------- verification loss -------------------------
    sample = "pair"
    adapt_alpha = AdaptParam("linear", 3e-4, 6e-3)
    veri = "spring"
    energy_fn = "euc"
    thres = None

    # # ------------------------- webface -------------------------
    deep_net = IntFace(channel, row, column, cate_num, old_model, veri, energy_fn, thres, sample, batch_sze, opt)
    deep_net.train(train_lst, train_storage, new_model, plot_file, train_steps, batch_sze, adapt_alpha)
    deep_net.vali(vali_lst, test_storage, batch_sze)

    # # ------------------------- verification & joint Bayesian-------------------------
    pair_lst_file = "../../../../data/LFW/files/pair_webNorm_100*100.pickle"
    veri_train_path = "../../../../data/WebFaceNorm/files/mirror1024_100*100_gray.pickle"

    with open(pair_lst_file, 'rb') as f:
        pickle.load(f)
        pair_lst = pickle.load(f)

    with open(veri_train_path, 'rb') as f:
        pickle.load(f)
        veri_train_lst = pickle.load(f)

    veri_train_data = intnet.data.CategoricalData(veri_train_lst, "lazy", x_dtype=the.config.floatX)

    features = intnet.datapro.get_feature(veri_train_data, deep_net.feature_fn)

    feature_dim = deep_net.feature_dim
    feature_fn = deep_net.feature_fn

    # feature_dim = 300
    # pca = PCA(features, feature_dim, standardize=False)
    # feature_fn = lambda x: pca.project(deep_net.feature_fn(x))

    jb = JointBayesian(feature_fn, feature_dim, np.mean(features, 0))  # np.mean(features, 0)
    jb.veri(pair_lst)
    jb.joint_bayesian_EM(veri_train_data, cov_ini="lda", iter=0, thres=1e-6, inv_fn=np.linalg.pinv)
    jb.veri(pair_lst)
    jb.joint_bayesian_EM(veri_train_data, cov_ini="eye", iter=50, thres=1e-6, inv_fn=np.linalg.pinv)
    jb.veri(pair_lst)