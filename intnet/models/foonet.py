# Shared variables with float32 dtype are by default moved to the GPU memory space.
# THEANO_FLAGS(mode=FAST_RUN,device=gpu1,floatX=float32,dnn.conv.algo_bwd_filter=time_once,dnn.conv.algo_bwd=time_once,lib.cnmem=0.5
import pickle
import theano as the
import intnet
import intnet.layer as lay
import numpy as np

# the.config.dnn.conv.algo_fwd = "time_once"
# the.config.dnn.conv.algo_bwd = "time_once"

floatX = "float32"
the.config.floatX = floatX

train_file_path = "/home/guest/Desktop/dev/data/mnist/files/train.txt"
test_file_path = "/home/guest/Desktop/dev/data/mnist/files/test.txt"

print "reading training and test lists."
train_lst = intnet.data.read_caffe_lst(train_file_path)
test_lst = intnet.data.read_caffe_lst(test_file_path)

row, column, channel, cate_num = 28, 28, 1, 10

# preparing data
im_pro_fn = intnet.data.get_im_pro_fn(color_space="L")  # load image as grayscale
train_ds = intnet.data.CategoricalData(train_lst, "in_memory", process_data_fn=im_pro_fn, x_dtype=floatX)
test_ds = intnet.data.CategoricalData(test_lst, "lazy", process_data_fn=im_pro_fn, x_dtype=floatX)


# input/target layer
l_x = lay.Input((None, channel, row, column), floatX)
l_y = lay.Input((None,), "int32")

# hidden layers
l_h1 = lay.Activation(lay.BatchNorm(lay.FullyConnected(l_x, 100, no_shift=True)), "relu")
l_out = lay.Activation(lay.FullyConnected(l_h1, cate_num), "softmax")

# loss
l_xent = lay.CrossEntropy(l_out, l_y)

# an ffnn is essentially a function to be optimized, defined by the loss and related layers
net = intnet.ffnn.FFNNOptimize(l_xent)

# initialize
net.init_param("Glorot_heuristic")

# an optimizer instance has its own (shared) parameters that need to be updated every iteration
opt = intnet.optimize.get_optimizer("nesterov", intnet.com.AdaptParam('fixed', 0.001), intnet.com.AdaptParam('fixed', 0.9))
# output loss, parameter and gradient
train_fn = net.opt_fn([l_x, l_y], [l_xent, l_out.pred.params["b"], the.grad(l_xent.out_var, l_out.pred.params["b"])], opt)

# train
train_steps = 1e4
batch_sze = 24
for i, x, y in intnet.data.EpochBatchLauncher(train_ds, batch_sze, train_steps, shuffle=True):
    opt.update_learning_params(i, train_steps)
    loss, b, g = train_fn(x, y)
    if (i + 1)%100 == 0:
        print("Iter %d: Loss %g" % (i + 1, loss))

# output function
l_pred = lay.ArgMax(l_out)
pred_fn = intnet.ffnn.ffnn_fn(l_x, l_pred)

# predict
hits = 0.
for i, x, y in intnet.data.EpochBatchLauncher(train_ds, batch_sze):
    predict = pred_fn(x)
    hits += np.sum(predict == y)
print "training accuracy:", hits/train_ds.size

# predict
hits = 0.
for i, x, y in intnet.data.EpochBatchLauncher(test_ds, batch_sze):
    predict = pred_fn(x)
    hits += np.sum(predict == y)
print "test accuracy:", hits/test_ds.size

# persistence
print "serializing model parameters."
model_file = "foonet.pickle"
net.save_param(model_file)
