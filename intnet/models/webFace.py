import time
import intnet
import theano as the
from base import VeriNet
from intnet.layer import *


class WebFace(VeriNet):
    def __init__(self, channel, row, column, cate_num, old_model_filename, veri, energy_fn, thres, sample, batch_sze, opt):
        """
        initialize the network model and relevant functions
        :param old_model_filename: if given, will continue training on the old model, if not, will initialize a random model
        """
        VeriNet.__init__(self)
        the.config.floatX = "float32"
        print "building net."
        # input/target layer
        l_x = Input((None, channel, row, column), the.config.floatX)
        l_y = Input((None,), "int32")
        # network
        l_conv11 = Activation(BatchNorm(Conv2d(l_x, (3, 3), 32, name='Cov11', no_shift=True)), "relu")  # 98
        l_conv12 = Activation(BatchNorm(Conv2d(l_conv11, (3, 3), 64, name='Cov12', no_shift=True)), "relu")  # 96
        l_pool1 = Pool2d(l_conv12, "max", (2, 2))  # 48
        l_conv21 = Activation(BatchNorm(Conv2d(l_pool1, (3, 3), 64, pad=1, name='Cov21', no_shift=True)), "relu")  # 48
        l_conv22 = Activation(BatchNorm(Conv2d(l_conv21, (3, 3), 128, pad=1, name='Cov22', no_shift=True)), "relu") # 48
        l_pool2 = Pool2d(l_conv22, "max", (2, 2))  # 24
        l_conv31 = Activation(BatchNorm(Conv2d(l_pool2, (3, 3), 96, pad=1, name='Cov31', no_shift=True)), "relu")  # 24
        l_conv32 = Activation(BatchNorm(Conv2d(l_conv31, (3, 3), 192, pad=1, name='Cov32', no_shift=True)), "relu")  # 24
        l_pool3 = Pool2d(l_conv32, "max", (2, 2))  # 12
        l_conv41 = Activation(BatchNorm(Conv2d(l_pool3, (3, 3), 128, pad=1, name='Cov41', no_shift=True)), "relu")  # 12
        l_conv42 = Activation(BatchNorm(Conv2d(l_conv41, (3, 3), 256, pad=1, name='Cov42', no_shift=True)), "relu")  # 12
        l_pool4 = Pool2d(l_conv42, "max", (2, 2))  # 6
        l_conv51 = Activation(BatchNorm(Conv2d(l_pool4, (3, 3), 160, pad=1, name='Cov51', no_shift=True)), "relu")  # 6
        l_conv52 = Activation(BatchNorm(Conv2d(l_conv51, (3, 3), 320, pad=1, name='Cov52', no_shift=True)), "relu")  # 6
        l_pool5 = Pool2d(l_conv52, "average_inc_pad", (6, 6))  # 1
        l_flat = Flatten(l_pool5)
        l_drop = Dropout(l_flat, 0.4)  # 320
        l_fc = FullyConnected(l_drop, cate_num, name='FC6')
        l_soft = Activation(l_fc, "softmax")

        # loss/error
        l_xent = CrossEntropy(l_soft, l_y)
        if sample.lower() == "triplet":
            l_veri = TripletVerification(l_flat, l_y, veri, energy_fn, batch_sze, thres)
        elif sample.lower() == "pair":
            l_veri = Verification(l_flat, l_y, veri, energy_fn, batch_sze, thres)
        else:
            raise ValueError("no sampling methods matched.")
        l_alpha = Input((), the.config.floatX)
        l_regular = Regular([l_fc], "l2")
        l_loss = Merge([l_xent, l_veri, l_regular], "lin_comb", [1, l_alpha.in_var, 1e-4])

        # train function
        self.net = intnet.ffnn.ModelFFNN(l_loss)
        self.optimizer = intnet.optimize.get_optimizer(*opt)
        self.train_fn = self.net.train_fn([l_x, l_y, l_alpha], [l_loss, l_xent, l_veri, l_regular], self.optimizer)

        # validation function
        # l_soft.pred = l_pool5  # skip dropout layer (not necessary any more
        l_pred = ArgMax(l_soft)
        l_acc = Accuracy(l_pred, l_y, "hits")
        self.test_fn = self.net.output_fn([l_x, l_y], [l_acc, l_pred])

        # feature function
        self.feature_fn = self.net.output_fn(l_x, l_flat)
        self.feature_dim = 320

        # load/init model
        self.net.init_param(old_model_filename)

        self.sampler = sample

#     def train(self, train_lst, train_storage, new_model_filename, plot_file, train_steps, batch_sze, adapt_alpha):
#         """
#         train a deepid network
#         """
#         print "loading training data."
#         train_dat = intnet.train.CategoricalData(train_lst, train_storage, x_dtype=the.config.floatX)
#
#         s = time.time()
#         for i, x, y in intnet.train.SplitBatchLauncher(batch_sze, train_dat, shuffle=True, steps=train_steps):
#             # update learning parameters
#             self.optimizer.update_learning_params(i, train_steps)
#             # change alpha gradually
#             xent, veri, regu, loss = self.train_fn(x, y, adapt_alpha.get_val(i, train_steps))
#             print "Iter %d: Loss %g = %g + %g+ %g" % (i + 1, loss, xent, veri, regu)
#         e = time.time()
#         print "time elapsed:", e - s
#
#         # training error
#         total_hits = 0
#         for i, x, y in intnet.train.SplitBatchLauncher(batch_sze, train_dat):
#             _, hits = self.valid_fn(x, y)
#             total_hits += hits
#         print "training acc:", "hits:", total_hits, "/", train_dat.size, float(total_hits) / train_dat.size
#
#         # persistence
#         print "serializing training and test lists."
#         self.webface_net.save_param(new_model_filename)
#         self.webface_net.plot(['xent', 'veri', 'loss'], plot_file)
#
#     def vali(self, vali_lst, vali_storage, batch_sze):
#         # predict
#         test_dat = intnet.train.CategoricalData(vali_lst, vali_storage, x_dtype=the.config.floatX)
#         total_hits = 0
#         for i, x, y in intnet.train.SplitBatchLauncher(batch_sze, test_dat):
#             hits, predict = self.valid_fn(x, y)
#             total_hits += hits
#         print "test acc:", "hits:", total_hits, "/", test_dat.size, float(total_hits) / test_dat.size
#
#
# if __name__ == "__main__":
#     # THEANO_FLAGS='mode=FAST_RUN,device=gpu1,floatX=float32,dnn.conv.algo_fwd=time_once,dnn.conv.algo_bwd=time_once,lib.cnmem=0.8'
#     # PYTHONPATH=/home/guest/Desktop/dev/git/intnet python deepidToy.py
#     # Shared variables with float32 dtype are by default moved to the GPU memory space.
#     import pickle
#     import intnet
#     from intnet.com import AdaptParam
#
#     # # ------------------------- split training -------------------------
#     train_path = "../../../../data/WebFaceNorm/files/mirror10575_100*100_gray_0.9.pickle"
#     vali_path = "../../../../data/WebFaceNorm/files/mirror10575_100*100_gray_0.1.pickle"
#
#     print "reading training and test lists."
#     with open(train_path, 'rb') as f:
#         row, column, channel, cate_num = pickle.load(f)
#         train_lst = pickle.load(f)
#     with open(vali_path, 'rb') as f:
#         pickle.load(f)
#         vali_lst = pickle.load(f)
#     # # ------------------------- split training -------------------------
#
#     # train_storage = "lazy"
#     train_storage = "in_memory"
#     test_storage = "lazy"
#
#     # results
#     old_model = None
#     # old_model = "webface.4.pickle"
#     new_model = "webface.6.pickle"
#     plot_file = "curves1.png"
#
#     # parameters
#     train_steps = 1e6
#     batch_sze = 32
#
#     adapt_lr = AdaptParam("linear", 1e-3, 1e-5)
#     # adapt_lr = AdaptParam("step", [(0.8, 1e-3), (1, 1e-4)])
#     adapt_m = AdaptParam("fixed", 0.9)
#     opt = ("my_ada", adapt_lr, adapt_m)
#
#     # # ------------------------- verification loss -------------------------
#     sample = "pair"
#     adapt_alpha = AdaptParam("linear", 6e-5, 6e-3)
#     veri = "spring"
#     energy_fn = "euc"
#     thres = None
#
#     # # ------------------------- webface -------------------------
#     deep_net = WebFace(channel, row, column, cate_num, old_model, veri, energy_fn, thres, sample, batch_sze, opt)
#     deep_net.train(train_lst, train_storage, new_model, plot_file, train_steps, batch_sze, adapt_alpha)
#     deep_net.vali(vali_lst, test_storage, batch_sze)
