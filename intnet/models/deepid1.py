import time
import intnet
import theano as the


class Deepid1:
    def __init__(self, channel, row, column, cate_num, old_model):
        """
        initialize a deepid network model and relevant functions
        :param old_model: if given, will continue training on the old model, if not, will initialize a random model
        """
        the.config.floatX = "float32"
        print "building net."
        # input/target layer
        l_x = intnet.layer.Input((None, channel, row, column), the.config.floatX)
        l_y = intnet.layer.Input((None,), "int32")
        # network
        l_conv1 = intnet.layer.Conv2d(l_x, "relu", (4, 4), 20)
        l_pool1 = intnet.layer.Pool2d(l_conv1, "max", (2, 2))
        l_conv2 = intnet.layer.Conv2d(l_pool1, "relu", (3, 3), 40)
        l_pool2 = intnet.layer.Pool2d(l_conv2, "max", (2, 2))
        l_local3 = intnet.layer.LocalConv2d(l_pool2, "relu", (3, 3), 60, (2, 2))
        l_pool3 = intnet.layer.Pool2d(l_local3, "max", (2, 2))
        l_local4 = intnet.layer.LocalConv2d(l_pool3, "relu", (2, 2), 80, (1, 1))
        l_concat = intnet.layer.FlatConcat([l_pool3, l_local4])
        l_deepid = intnet.layer.FullyConnected(l_concat, "relu", 160)
        l_soft = intnet.layer.FullyConnected(l_deepid, "softmax", cate_num)

        # loss/error
        l_xent = intnet.layer.CrossEntropy(l_soft, l_y)

        self.deepid_net = intnet.ffnn.ModelFFNN(l_soft)

        # deepid layer function
        self.deepid_fn = self.deepid_net.output_fn(l_x, l_deepid)

        # train function
        l_learn_rate = intnet.layer.Input((), the.config.floatX)
        l_momentum = intnet.layer.Input((), the.config.floatX)
        opt = intnet.com.Optimizer("momentum", l_learn_rate.in_var, l_momentum.in_var)
        self.train_fn = self.deepid_net.train_fn([l_x, l_y, l_learn_rate, l_momentum], l_xent, l_xent, opt)

        # opt = intnet.com.Optimizer("sgd", 1e-2)
        # train_fn = deepid_net.train_fn([l_x, l_y], l_xent, l_xent, opt)

        # validation function
        l_pred = intnet.layer.ArgMax(l_soft)
        l_acc = intnet.layer.Accuracy(l_pred, l_y, "hits")
        self.valid_fn = self.deepid_net.output_fn([l_x, l_y], [l_pred, l_acc])

        # load model
        if old_model is None:
            self.deepid_net.init_param("Glorot_heuristic")
        else:
            self.deepid_net.load_param(old_model)

    def train(self, train_lst, train_storage, new_model, plot_file, train_steps, batch_sze, l_s, l_e, m_s, m_e):
        """
        train a deepid network
        """
        print "loading training data."
        train_dat = intnet.data.CategoricalData(train_lst, train_storage, x_dtype=the.config.floatX)

        s = time.time()
        for i, x, y in intnet.data.RandomBatchLauncher(train_steps, batch_sze, train_dat):
            # increase momentum gradually
            loss = self.train_fn(x, y, l_s+(l_e-l_s)*i/train_steps, m_s+(m_e-m_s)*i/train_steps)
            # loss = train_fn(x, y)
            print "Iter %d: Loss %g" % (i + 1, loss)
        e = time.time()
        print "time elapsed:", e - s

        # training error
        total_hits = 0
        for i, x, y in intnet.data.SplitBatchLauncher(batch_sze, train_dat):
            _, hits = self.valid_fn(x, y)
            total_hits += hits
        print "training acc:", "hits:", total_hits, "/", train_dat.size, float(total_hits) / train_dat.size

        # persistence
        print "serializing training and test lists."
        self.deepid_net.save_param(new_model)
        self.deepid_net.plot(['loss'], plot_file)

    def vali(self, vali_lst, vali_storage, batch_sze):
        # predict
        test_dat = intnet.data.CategoricalData(vali_lst, vali_storage, x_dtype=the.config.floatX)
        total_hits = 0
        for i, x, y in intnet.data.SplitBatchLauncher(batch_sze, test_dat):
            predict, hits = self.valid_fn(x, y)
            total_hits += hits
            # print predict
            # print y
        print "test acc:", "hits:", total_hits, "/", test_dat.size, float(total_hits) / test_dat.size
