# coding=utf-8
from testScript import *

print "START"

# hi = np.logspace(-0.8, -4, num=5)
lo = (6e-4,)

# lo = np.linspace(0.0075, 0.01, 10)
# hi = (0.075,)
# hi = lo*10

paras = []

## Grid
# for i in hi:
#     for j in lo:
#         if i > j:
#             paras.append(np.asarray((j, i)))

paras.append((0, 0, 'whole', 0.98, 'pair', 1e5, 1e-4))
paras.append((0, 0, 'whole', 0.99, 'triplet', 1e5, 1e-4))
paras.append((3e-4, 3e-3, 'whole', 0.98, 'pair', 1e5, 1e-4))
paras.append((3e-4, 3e-3, 'whole', 0.99, 'triplet', 1e5, 1e-4))
paras.append((1e-4, 1e-3, 'whole', 0.98, 'pair', 1e5, 1e-4))
paras.append((1e-4, 1e-3, 'whole', 0.99, 'triplet', 1e5, 1e-4))
paras.append((5e-5, 1e-3, 'whole', 0.98, 'pair', 1e5, 1e-4))
paras.append((5e-5, 1e-3, 'whole', 0.99, 'triplet', 1e5, 1e-4))

# paras = ((6e-4, 6e-3, 'whole', 0.98, 'pair'), )

# print lo
# print hi
print paras
print len(paras)

res = []
output = open('paras.txt', 'a')

for i, para in zip(range(len(paras)), paras):
    # para = tuple(para)
    print "-" * 50
    print "%s / %s : %s" % (i + 1, len(paras), para)
    print "-" * 50
    one = (para, para_test(*para))
    res.append(one)
    print one
    print "-" * 50
    out_str = "%s / %s : %s\n" % (i + 1, len(paras), one)
    output.writelines(out_str)
    output.flush()

output.close()
