import time
from functools import partial
import theano as the
import intnet


# ------------------------- model hyper-parameters -------------------------
class GenderWebFace:
    def __init__(self, channel, row, column, cate_num, old_model_filename, opt):
        """
        initialize the network model and relevant functions
        :param old_model_filename: if given, will continue training on the old model, if not, will initialize a random model
        """
        the.config.floatX = "float32"
        print "building net."
        # input/target layer
        l_x = intnet.layer.Input((None, channel, row, column), the.config.floatX)
        l_y = intnet.layer.Input((None,), "int32")
        # network
        l_conv11 = intnet.layer.Conv2d(l_x, "relu", (3, 3), 32, name='Cov11')  # 98
        l_conv12 = intnet.layer.Conv2d(l_conv11, "relu", (3, 3), 64, name='Cov12')  # 96
        l_pool1 = intnet.layer.Pool2d(l_conv12, "max", (2, 2))  # 48
        l_conv21 = intnet.layer.Conv2d(l_pool1, "relu", (3, 3), 64, pad=1, name='Cov21')  # 48
        l_conv22 = intnet.layer.Conv2d(l_conv21, "relu", (3, 3), 128, pad=1, name='Cov22')  # 48
        l_pool2 = intnet.layer.Pool2d(l_conv22, "max", (2, 2))  # 24
        l_conv31 = intnet.layer.Conv2d(l_pool2, "relu", (3, 3), 96, pad=1, name='Cov31')  # 24
        l_conv32 = intnet.layer.Conv2d(l_conv31, "relu", (3, 3), 192, pad=1, name='Cov32')  # 24
        l_pool3 = intnet.layer.Pool2d(l_conv32, "max", (2, 2))  # 12
        l_conv31 = intnet.layer.Conv2d(l_pool3, "relu", (3, 3), 128, pad=1, name='Cov41')  # 12
        l_conv32 = intnet.layer.Conv2d(l_conv31, "relu", (3, 3), 256, pad=1, name='Cov42')  # 12
        l_pool4 = intnet.layer.Pool2d(l_conv32, "max", (2, 2))  # 6
        l_conv41 = intnet.layer.Conv2d(l_pool4, "relu", (3, 3), 160, pad=1, name='Cov51')  # 6
        l_conv42 = intnet.layer.Conv2d(l_conv41, "relu", (3, 3), 320, pad=1, name='Cov52')  # 6
        l_pool5 = intnet.layer.Pool2d(l_conv42, "average_inc_pad", (6, 6))  # 1
        l_flat = intnet.layer.Flatten(l_pool5)  # 320
        l_drop = intnet.layer.Dropout(l_flat, 0.5)
        l_soft = intnet.layer.FullyConnected(l_drop, "softmax", cate_num, name='FC6_2')

        # loss/error
        l_xent = intnet.layer.CrossEntropy(l_soft, l_y)
        l_regular = intnet.layer.Regular([l_soft], "l2")
        l_loss = intnet.layer.LinearComb([l_xent, l_regular], [1, 5e-4])

        # train function
        self.webface_net = intnet.ffnn.ModelFFNN(l_loss)
        self.optimizer = intnet.optimize.get_optimizer(*opt)
        self.train_fn = self.webface_net.train_fn([l_x, l_y], [l_loss, l_xent, l_regular], self.optimizer)

        # validation function
        l_soft.pred = l_pool5  # skip dropout layer
        l_pred = intnet.layer.ArgMax(l_soft)
        l_acc = intnet.layer.Accuracy(l_pred, l_y, "hits")
        self.valid_fn = self.webface_net.output_fn([l_x, l_y], [l_acc, l_pred])

        # deepid layer function
        self.feature_fn = self.webface_net.output_fn(l_x, l_flat)
        self.feature_dim = 320

        # load/init model
        self.webface_net.init_param(old_model_filename)

    def train(self, train_lst, process_tag_fn, train_storage, new_model_filename, plot_file, train_steps, batch_sze):
        """
        train a deepid network
        """
        print "loading training data."
        train_dat = intnet.data.BinaryData(train_lst, train_storage, process_tag_fn, x_dtype=the.config.floatX)

        s = time.time()
        for i, x, y in intnet.data.EpochBatchLauncher(train_dat, batch_sze, steps=train_steps, shuffle=True):
            # update learning parameters
            self.optimizer.update_learning_params(i, train_steps)
            # change alpha gradually
            loss, xent, regu = self.train_fn(x, y)
            print "Iter %d: Loss %g = %g + %g" % (i + 1, loss, xent, regu)
        e = time.time()
        print "time elapsed:", e - s

        # training error
        total_hits = 0
        for i, x, y in intnet.data.EpochBatchLauncher(train_dat, batch_sze):
            hits, _ = self.valid_fn(x, y)
            total_hits += hits
        print "training acc:", "hits:", total_hits, "/", train_dat.size, float(total_hits) / train_dat.size

        # persistence
        print "serializing training and test lists."
        self.webface_net.save_param(new_model_filename)
        self.webface_net.plot(['xent', 'regu', 'loss'], plot_file)

    def vali(self, vali_lst, process_tag_fn, vali_storage, batch_sze):
        # predict
        test_dat = intnet.data.BinaryData(vali_lst, vali_storage, process_tag_fn, x_dtype=the.config.floatX)
        total_hits = 0
        for i, x, y in intnet.data.EpochBatchLauncher(test_dat, batch_sze):
            hits, predict = self.valid_fn(x, y)
            total_hits += hits
        print "test acc:", "hits:", total_hits, "/", test_dat.size, float(total_hits) / test_dat.size


# the script
if __name__ == "__main__":
    # Shared variables with float32 dtype are by default moved to the GPU memory space.
    import pickle
    import intnet
    from intnet.com import AdaptParam
    from intnet.datapro.launch import *

    print "reading training and test lists."

    # # ------------------------- split training set -------------------------
    # train_path = "../../../../data/celebANorm/files/ac202599_100*100_gray_crop_train.pickle"
    # vali_path = "../../../../data/celebANorm/files/ac202599_100*100_gray_crop_test.pickle"
    #
    # with open(train_path, 'rb') as f:
    #     row, column, channel, attr_name_lst = pickle.load(f)
    #     train_lst = pickle.load(f)
    # with open(vali_path, 'rb') as f:
    #     pickle.load(f)
    #     vali_lst = pickle.load(f)
    #
    # # # ------------------------- split training set -------------------------
    # # # file_path = "../../../../data/celebANorm/files/mirror10_100*100_gray_crop.pickle"
    # # file_path = "../../../../data/celebANorm/files/mirror10000_100*100_gray_crop.pickle"
    # # with open(file_path, 'rb') as f:
    # #     row, column, channel, attr_name_lst = pickle.load(f)
    # #     data_lst = pickle.load(f)
    # # train_lst, vali_lst = intnet.train.train_test_split(data_lst, test_pro=0.2)
    #
    # # # ------------------------- split training set -------------------------
    # process_tag_fn = partial(get_attr_by_idx, idx=attr_name_lst.index("Male"))


    # # ------------------------- split training set -------------------------
    # train_path = "../../../../data/celebANorm/files/ac202599_100*100_gray_crop_train.pickle"
    # vali_path = "../../../../data/celebANorm/files/ac202599_100*100_gray_crop_test.pickle"
    #
    # with open(train_path, 'rb') as f:
    #     row, column, channel, attr_name_lst = pickle.load(f)
    #     train_lst = pickle.load(f)
    # with open(vali_path, 'rb') as f:
    #     pickle.load(f)
    #     vali_lst = pickle.load(f)

    # ------------------------- split training set -------------------------
    file_path = "../../../../data/foreigner/files/ac2_100*100_gray_12855.pickle"
    with open(file_path, 'rb') as f:
        row, column, channel, cate_num = pickle.load(f)
        data_lst = pickle.load(f)
    train_lst, vali_lst = intnet.data.train_test_split(data_lst, 0.9)

    # # ------------------------- split training set -------------------------
    process_tag_fn = lambda x:x

    # # ------------------------- training parameters -------------------------
    # train_storage = "lazy"
    train_storage = "in_memory"
    test_storage = "lazy"

    # results
    old_model = "webface.6.pickle"
    # old_model = None
    new_model = "g_webface.2.pickle"
    plot_file = "curves1.png"

    # parameters
    train_steps = 3e3
    batch_sze = 64

    adapt_lr = AdaptParam("linear", 1e-3, 3e-5)
    # adapt_lr = AdaptParam("step", [(0.8, 1e-3), (1, 1e-4)])
    adapt_m = AdaptParam("linear", 0.9, 0.9)
    opt = ("adam", adapt_lr, adapt_m)

    # # ------------------------- webface -------------------------
    deep_net = GenderWebFace(channel, row, column, 2, old_model, opt)
    deep_net.train(train_lst, process_tag_fn, train_storage, new_model, plot_file, train_steps, batch_sze)
    deep_net.vali(vali_lst, process_tag_fn, test_storage, batch_sze)
