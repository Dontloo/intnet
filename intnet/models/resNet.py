import time
import intnet
import theano as the
from base import VeriNet
from intnet.layer import *


class ResNet(VeriNet):
    def __init__(self, channel, row, column, cate_num, old_model_filename, veri, energy_fn, thres, sample, batch_sze, opt):
        """
        initialize the network model and relevant functions
        :param old_model_filename: if given, will continue training on the old model, if not, will initialize a random model
        """
        VeriNet.__init__(self)
        the.config.floatX = "float32"
        print "building net."
        # input/target layer
        l_x = Input((None, channel, row, column), the.config.floatX)
        l_y = Input((None,), "int32")
        # network
        l_conv1 = Activation(BatchNorm(Conv2d(l_x, (3, 3), 48, name='Conv1', no_shift=True)), "relu")  # 98
        l_conv2 = Activation(BatchNorm(Conv2d(l_conv1, (3, 3), 96, name='Conv2', no_shift=True)), "relu")  # 96
        l_res11 = ResBottle(l_conv2, 64, 128, "Res11", mode="b", stride=(2, 2))  # 48
        l_res12 = ResBottle(l_res11, 64, 128, "Res12")  # 48
        l_res13 = ResBottle(l_res12, 64, 128, "Res13")  # 48
        l_res21 = ResBottle(l_res13, 96, 192, "Res21", mode="b", stride=(2, 2))  # 24
        l_res22 = ResBottle(l_res21, 96, 192, "Res22")  # 24
        l_res23 = ResBottle(l_res22, 96, 192, "Res23")  # 24
        l_res31 = ResBottle(l_res23, 128, 256, "Res31", mode="b", stride=(2, 2))  # 12
        l_res32 = ResBottle(l_res31, 128, 256, "Res32")  # 12
        l_res33 = ResBottle(l_res32, 128, 256, "Res33")  # 12
        l_res41 = ResBottle(l_res33, 160, 320, "Res41", mode="b", stride=(2, 2))  # 6
        l_res42 = ResBottle(l_res41, 160, 320, "Res42",)  # 6
        l_res43 = ResBottle(l_res42, 160, 320, "Res43",)  # 6

        l_pool5 = Pool2d(l_res43, "average_inc_pad", (6, 6))  # 1
        l_flat = Flatten(l_pool5)
        l_drop = Dropout(l_flat, 0.4)
        l_fc = FullyConnected(l_drop, cate_num, name='FC6')
        l_soft = Activation(l_fc, "softmax")

        # loss/error
        l_xent = CrossEntropy(l_soft, l_y, noise=0, cate_num=cate_num)
        if sample.lower() == "triplet":
            l_veri = TripletVerification(l_flat, l_y, veri, energy_fn, batch_sze, thres, margin=0.3)
        elif sample.lower() == "pair":
            l_veri = Verification(l_flat, l_y, veri, energy_fn, batch_sze, thres, margin=0.3)
        else:
            raise ValueError("no sampling methods matched.")
        l_alpha = Input((), the.config.floatX)
        l_regular = Regular([l_fc], "l2")
        l_loss = Merge([l_xent, l_veri, l_regular], "lin_comb", [1, l_alpha.in_var, 1e-4])

        # train function
        self.net = intnet.ffnn.ModelFFNN(l_loss)
        self.optimizer = intnet.optimize.get_optimizer(*opt)
        self.train_fn = self.net.train_fn([l_x, l_y, l_alpha], [l_loss, l_xent, l_veri, l_regular], self.optimizer)

        # validation function
        # l_soft.pred = l_pool5  # skip dropout layer (not necessary any more
        l_pred = ArgMax(l_soft)
        l_acc = Accuracy(l_pred, l_y, "hits")
        self.test_fn = self.net.output_fn([l_x, l_y], [l_acc, l_pred])

        # feature function
        self.feature_fn = self.net.output_fn(l_x, l_flat)
        self.feature_dim = 320

        # load/init model
        self.net.init_param(old_model_filename)

        self.sampler = sample