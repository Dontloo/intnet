import pickle
import numpy as np
from sklearn import svm
import intnet
import theano as the
from genderWebFace import GenderWebFace
from sklearn.metrics import confusion_matrix
import os
import shutil

# # ---------------------- data augmentation ----------------------
# file_path = "../../../../data/foreigner/files/ac2_100*100_gray_10000.pickle"
# with open(file_path, 'rb') as f:
#     row, column, channel, cate_num = pickle.load(f)
#     data_lst = pickle.load(f)
#
# # ---------------------- extract feature ----------------------
# old_model = "g_webface.2.pickle"
#
# adapt_lr = intnet.com.AdaptParam("linear", 1e-3, 3e-5)
# adapt_m = intnet.com.AdaptParam("linear", 0.9, 0.9)
# opt = ("adam", adapt_lr, adapt_m)
#
# dat = intnet.train.CategoricalData(data_lst, "lazy", x_dtype=the.config.floatX)
# deep_net = GenderWebFace(channel, row, column, 2, old_model, opt)
# features, labels = intnet.data.get_feature(dat, deep_net.feature_fn)
# print labels.shape
# print features.shape
#
# feature_file = '../../../../data/foreigner/sdk/foreign_rec_webface.pickle'
# with open(feature_file, 'wb') as f:
#     pickle.dump(features, f, pickle.HIGHEST_PROTOCOL)
#     pickle.dump(labels, f, pickle.HIGHEST_PROTOCOL)

# ---------------------- load feature ----------------------
def load_data_permute(feature_file, shuffle=True):
    with open(feature_file) as f:
        dat = pickle.load(f)
        lab = pickle.load(f)
        print dat.shape, lab.shape
    if shuffle:
        idx_permute = np.random.permutation(len(lab))
        dat = dat[idx_permute]
        lab = lab[idx_permute]
    return dat, lab

# ---------------------- load feature ----------------------
dat_c, lab_c = load_data_permute('../../../../data/foreigner/sdk/chinese_feature.pickle')
dat_f, lab_f = load_data_permute('../../../../data/foreigner/sdk/foreign_feature.pickle')
dat_l, lab_l = load_data_permute('../../../../data/laomu/sdk/983_clean.pickle')

# ---------------------- train ----------------------
# 22173, 22855
train_x = np.concatenate([dat_c[:1300], dat_f[:1300-983], dat_l[:983]])
train_y = np.concatenate([lab_c[:1300], lab_f[:1300-983], lab_l[:983]])

clf = svm.SVC(C=1e2, kernel='rbf', gamma='auto', verbose=True)
# clf = svm.SVC(C=1000, verbose=True)
clf.fit(train_x, train_y)

# ---------------------- test ----------------------
test_x = np.concatenate([dat_c[:983], dat_l[:983]])
test_y = np.concatenate([lab_c[:983], lab_l[:983]])
print clf.score(train_x, train_y)
print clf.score(test_x, test_y)

pickle.dump(clf, open("svm.p", "wb"))


# ---------------------- predict ----------------------
def load_data(arg_feature_file):
    with open(arg_feature_file) as f:
        dat = pickle.load(f)
        lab = pickle.load(f)
        names = pickle.load(f)
        print dat.shape, lab.shape, names.shape
    return dat, lab, names


def fldr_out(in_fldr, out_fldr_0, out_fldr_1, names, labs):
    for i in range(len(names)):
        if labs[i] == 1:
            shutil.copy(os.path.join(in_fldr, names[i]), os.path.join(out_fldr_1, names[i]))
        else:
            shutil.copy(os.path.join(in_fldr, names[i]), os.path.join(out_fldr_0, names[i]))


def file_out(out_file, names, labs):
    with open(out_file, 'w') as f:
        for i in range(len(names)):
            '{0:{width}}'.format(333, width=5)
            f.write(str(names[i]).ljust(16)+str(int(labs[i]))+"\n")


def k_largest(lst, k):
    return sorted(lst, reverse=True)[k-1]

feature_file = "../../../../data/laomu/sdk/983_clean.pickle"
model_file = "svm.p"

clf = pickle.load(open(model_file, "rb"))
test_x, test_y, test_names = load_data(feature_file)

score = clf.decision_function(test_x)
thres = k_largest(score, 973)
pred_y = np.ceil(np.tanh(score-thres))
print confusion_matrix(test_y, pred_y)

pic_fldr = "/home/guest/Desktop/raw_data/laomu/color100sdk/"
l_out_fldr = "/home/guest/Desktop/tmp/wz/"
c_out_fldr = "/home/guest/Desktop/tmp/hz/"
file_out_fldr = "/home/guest/Desktop/tmp/predictions"
fldr_out(pic_fldr, c_out_fldr, l_out_fldr, test_names, pred_y)
file_out(file_out_fldr, test_names, pred_y)
