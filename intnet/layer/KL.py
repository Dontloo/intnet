from .base import Layer
import theano.tensor as ten
import theano as the


class KL(Layer):
    """
    KL divergence between two Gaussians for VAE,
    see https://github.com/fchollet/keras/blob/master/examples/variational_autoencoder.py#L45
    """
    multi_pred = True

    def __init__(self, l_mean, l_log_var, noise=0, cate_num=None, **kwargs):
        Layer.__init__(self)
        # incoming layer
        self.pred = [l_mean, l_log_var]
        # output shape
        self.out_shape = (1,)
        # keyword arguments
        self.name = kwargs.get("name", None)

    def get_out(self, train):
        return - 0.5 * ten.mean(ten.sum(1 + self.pred[1].out_var - ten.square(self.pred[0].out_var) - ten.exp(self.pred[1].out_var), axis=1))
