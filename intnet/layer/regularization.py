from .base import Layer
import theano.tensor as ten


class Regular(Layer):
    """
    lp norms will have a large value if there are many parameters to be normalized.
    so the weight of the regularization term needs to be adjusted accordingly.
    """
    multi_pred = True

    def __init__(self, predecessor, norm, **kwargs):
        Layer.__init__(self)
        # incoming layer
        self.pred = predecessor
        # output shape
        self.out_shape = (1,)
        # l0: number of non-zero parameters
        if norm.lower() == "l1":
            # tensor variable overloads the `TensorVariable.__abs__` operator
            self.norm = lambda x: ten.sum(abs(x))
        elif norm.lower() == "l2":
            self.norm = lambda x: ten.sum(x**2)
        # keyword arguments
        self.name = kwargs.get("name", None)

    def get_out(self, train):
        params = []
        for layer in self.pred:
            for key in layer.params:
                # regularization can be only applied to the "w" term
                # http://neuralnetworksanddeeplearning.com/chap3.html#overfitting_and_regularization
                if key.lower() == "w":
                    params.append(layer.params[key])
        return sum(map(self.norm, params))
