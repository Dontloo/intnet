from .base import Layer
import theano.tensor as ten


class ArgMax(Layer):
    """
    argMax layer
    perform aggMax along the second axis
    essentially just an activation function
    """
    def __init__(self, predecessor, **kwargs):
        Layer.__init__(self)
        # incoming layer
        self.pred = predecessor
        # output shape
        self.out_shape = (None,)
        # keyword arguments
        self.name = kwargs.get("name", None)

    def get_out(self, train):
        return ten.argmax(self.pred.out_var, axis=1)
