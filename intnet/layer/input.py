from .base import Layer
import theano.tensor as ten


class Input(Layer):
    has_input = True

    def __init__(self, in_shape, dtype, **kwargs):
        Layer.__init__(self)
        # input shape
        self.in_shape = in_shape
        # input variable
        self.in_var = ten.TensorType(dtype, [False]*len(in_shape))()
        # output shape
        self.out_shape = self.in_shape
        # keyword arguments
        self.name = kwargs.get("name", None)

    def get_out(self, train):
        return self.in_var
