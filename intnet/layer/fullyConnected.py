from .base import Layer
import theano.tensor as ten
import theano as the
import numpy as np
from intnet.com import mul_reduce


class FullyConnected(Layer):
    """
    fully-connected layer
    """
    has_param = True

    def __init__(self, predecessor, unit_num, no_shift=False, **kwargs):
        Layer.__init__(self)
        # incoming layer
        self.pred = predecessor
        # number of hidden units (neurons
        self.unit_num = unit_num
        # dependency relations: out_shape <- param_shape <- params <- output
        # output shape
        self.out_shape = (self.pred.out_shape[0], self.unit_num)
        # parameters
        self.params = {'w': the.shared(np.zeros(self.get_w_shape(), dtype=the.config.floatX))}
        # whether to have the shift parameter b, as it's no need if say batch normalization is applied
        if not no_shift:
            self.params["b"] = the.shared(np.zeros((self.unit_num,), dtype=the.config.floatX))
        # keyword arguments
        self.name = kwargs.get("name", None)

    def get_w_shape(self):
        return mul_reduce(self.pred.out_shape[1:]), self.unit_num

    def get_out(self, train):
        tmp = self.pred.out_var
        # flatten input of more than 2 dimensions
        if len(self.pred.out_shape) > 2:
            tmp = tmp.flatten(2)
        ret = ten.dot(tmp, self.params["w"])
        if "b" in self.params:
            ret += self.params["b"].dimshuffle('x', 0)
        return ret
