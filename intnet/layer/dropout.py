from .base import Layer
import theano as the
import numpy as np
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams


class Dropout(Layer):
    """
    dropout layer
    only support non-convolutional layers currently,
    so the dropout layer output will be divided by a factor of (1-p),
    to compensate for the fact that 1/(1-p) as many of them are active,
    for the reason why scaling the output in the training function instead of the test function, see:
    http://stats.stackexchange.com/questions/205932/dropout-scaling-the-activation-versus-inverting-the-dropout/205965#205965
    """

    def __init__(self, predecessor, p, **kwargs):
        Layer.__init__(self)
        # predecessor
        self.pred = predecessor
        # dropping probability
        self.p = p
        # output shape
        self.out_shape = self.pred.out_shape
        # keyword arguments
        self.name = kwargs.get("name", None)

    def get_out(self, train):
        if train:
            # need to broadcast according to input dimensions
            dim_pattern = ['x', 0] + ['x' for _ in range(len(self.pred.out_shape)-2)]
            mask = random_binomial((self.pred.out_shape[1],), 1-self.p).dimshuffle(dim_pattern)
            # divid by a factor of (1-p), to compensate for the fact that 1/(1-p) as many of them are active during test
            return self.pred.out_var*mask/(1-self.p)
        else:
            return self.pred.out_var


def random_binomial(shape, p, std=1.0, dtype=the.config.floatX, seed=None):
    if seed is None:
        seed = np.random.randint(1, 2147462579)  # magic number copied from lasagne
    rng = RandomStreams(seed=seed)
    return rng.binomial(size=shape, p=p, dtype=dtype)
