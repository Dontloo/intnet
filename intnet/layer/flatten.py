from .base import Layer
from intnet.com import mul_reduce


class Flatten(Layer):
    """
    flatten the input layer
    """

    def __init__(self, predecessor, **kwargs):
        Layer.__init__(self)
        self.pred = predecessor
        # output shape
        self.out_shape = self.get_out_shape()
        # keyword arguments
        self.name = kwargs.get("name", None)

    def get_out_shape(self):
        return self.pred.out_shape[0], mul_reduce(self.pred.out_shape[1:])

    def get_out(self, train):
        # flatten input of more than 2 dimensions
        if len(self.pred.out_shape) <= 2:
            raise TypeError("Error: can only flatten variables with dimension > 2.")
        return self.pred.out_var.flatten(2)
