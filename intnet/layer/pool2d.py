from .base import Layer
from theano.sandbox.cuda.dnn import dnn_pool
import theano.tensor as ten


class Pool2d(Layer):
    """
    2D pooling layer, based on cudnn
    """
    def __init__(self, predecessor, mode, pool_shape=(2, 2), **kwargs):
        Layer.__init__(self)
        # predecessor
        self.pred = predecessor
        # pool size
        self.pool_shape = pool_shape
        # outputs
        self.out_shape = self.get_out_shape()
        # keyword arguments
        self.name = kwargs.get("name", None)
        # method {max, sum, average_inc_pad, average_exc_pad}
        if self.pool_shape == "global":
            if mode == "max":
                self.pool_fn = ten.max
            elif mode == "sum":
                self.pool_fn = ten.sum
            elif mode == "average":
                self.pool_fn = ten.mean
            else:
                raise ValueError("no function matched for global pooling")
        else:
            self.pool_fn = mode

    def get_out_shape(self):
        if self.pool_shape == "global":
            return self.pred.out_shape[:2]
        out_row = self.pred.out_shape[2] // self.pool_shape[0]
        out_col = self.pred.out_shape[3] // self.pool_shape[1]
        return self.pred.out_shape[:2] + (out_row, out_col)

    def get_out(self, train):
        if self.pool_shape == "global":
            return self.pool_fn(self.pred.out_var.flatten(3), axis=2)
        # cuda.dnn.dnn_pool is a lot faster than theano.tensor.signal.downsample.max_pool_2d
        # esp. in the case of average_inc_pad/average_exc_pad
        return dnn_pool(self.pred.out_var, self.pool_shape, stride=self.pool_shape, mode=self.pool_fn)
