from .base import Layer
import theano.tensor as ten
import theano as the


class CrossEntropy(Layer):
    """
    cross entropy layer, output the mean cross entropy of all data points
    """
    multi_pred = True

    def __init__(self, pred_layer, tar_layer, noise=0, cate_num=None, **kwargs):
        Layer.__init__(self)
        # incoming layer
        self.pred = [pred_layer, tar_layer]
        # output shape
        self.out_shape = (1,)
        # keyword arguments
        self.name = kwargs.get("name", None)
        self.noise = noise
        if noise != 0:
            self.cate_num = cate_num

    def get_out(self, train):
        predict = self.pred[0].out_var
        if self.noise == 0:
            target = self.pred[1].out_var
        else:
            target = self.noise/self.cate_num + (1-self.noise)*ten.extra_ops.to_one_hot(self.pred[1].out_var, self.cate_num, dtype=the.config.floatX)
        return ten.mean(ten.nnet.categorical_crossentropy(predict, target))
