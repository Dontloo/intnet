from .base import Layer
import theano.tensor as ten
import theano as the


class BinaryXent(Layer):
    """
    sum of squares error layer, output the mean SSE of all data points
    """
    multi_pred = True

    def __init__(self, pred_layer, tar_layer, **kwargs):
        Layer.__init__(self)
        # incoming layer
        self.pred = [pred_layer, tar_layer]
        # output shape
        self.out_shape = (1,)
        # keyword arguments
        self.name = kwargs.get("name", None)

    def get_out(self, train):
        # output = ten.clip(self.pred[0].out_var, 1e-8, 1.0 - 1e-8)
        return ten.mean(ten.sum(ten.nnet.binary_crossentropy(self.pred[0].out_var, self.pred[1].out_var), axis=1))
