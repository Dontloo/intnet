from .base import Layer
import theano as the
import numpy as np
import theano.tensor as ten
import theano.sandbox.cuda.dnn as dnn
import intnet.com.nonLinearity as lin


class Conv2d(Layer):
    """
    2D convolutional layer
    """
    has_param = True

    def __init__(self, predecessor, kernel_shape, filter_num, stride=(1, 1), pad='valid', no_shift=False, **kwargs):
        Layer.__init__(self)
        # predecessor
        self.pred = predecessor
        # kernel shape
        self.kernel_shape = kernel_shape
        # number of filters
        self.filter_num = filter_num
        # stride/subsample
        self.stride = stride
        # convolution border mode, only full and valid are supported by theano
        self.pad = pad
        # dependency relations: out_shape <- param_shape <- params <- output
        # output shape
        self.out_shape = self.get_out_shape()
        # parameters
        self.params = {'w': the.shared(np.zeros(self.get_w_shape(), dtype=the.config.floatX))}
        # whether to have the shift parameter b, as it's no need if say batch normalization is applied
        if not no_shift:
            self.params["b"] = the.shared(np.zeros((self.filter_num,), dtype=the.config.floatX))
        # keyword arguments
        self.name = kwargs.get("name", None)

    def get_out(self, train):
        # ret = ten.nnet.conv2d(input=self.pred.out_var,
        #                       filters=self.params["w"],
        #                       image_shape=self.pred.out_shape,
        #                       filter_shape=self.params["w"].get_value().shape,
        #                       border_mode=self.border_mode,
        #                       subsample=self.stride)
        ret = dnn.dnn_conv(img=self.pred.out_var,
                           kerns=self.params["w"],
                           subsample=self.stride,
                           border_mode=self.pad)
        if "b" in self.params:
            ret += self.params["b"].dimshuffle('x', 0, 'x', 'x')
        return ret

    def get_w_shape(self):
        return (self.filter_num, self.pred.out_shape[1]) + self.kernel_shape

    def get_out_shape(self):
        def get1d(in_len, kernel_len, stride, pad):
            if pad == 'valid':
                out_len = in_len - kernel_len + 1
            elif pad == 'full':
                out_len = in_len + kernel_len - 1
            elif pad > 0:
                out_len = in_len - kernel_len + 1 + 2*pad
            else:
                raise ValueError('Invalid pad for convolution')
            # This is the integer arithmetic equivalent to
            # np.ceil(output_length / stride)
            # see Lasagne https://github.com/Lasagne/Lasagne
            out_len = (out_len + stride - 1) // stride
            return out_len

        out_row = get1d(self.pred.out_shape[2], self.kernel_shape[0], self.stride[0], self.pad)
        out_col = get1d(self.pred.out_shape[3], self.kernel_shape[1], self.stride[1], self.pad)

        return self.pred.out_shape[0], self.filter_num, out_row, out_col
