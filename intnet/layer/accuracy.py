from .base import Layer
import theano.tensor as ten


class Accuracy(Layer):
    """
    output the accuracy/0-1 error/hits/miss
    """
    multi_pred = True  # multiple predecessors

    def __init__(self, predecessor, tar_layer, mode, **kwargs):
        Layer.__init__(self)
        # incoming layer
        self.pred = [predecessor, tar_layer]
        # mode (acc/err/hits/miss
        self.mode = mode
        # output shape
        self.out_shape = (1,)
        # keyword arguments
        self.name = kwargs.get("name", None)

    def get_out(self, train):
        predict = self.pred[0].out_var
        target = self.pred[1].out_var
        if target.ndim == 2:  # for one-hot
            target = ten.argmax(target, axis=1)

        if self.mode.lower() == "acc":
            return ten.mean(ten.eq(predict, target))
        elif self.mode.lower() == "hits":
            return ten.sum(ten.eq(predict, target))
        elif self.mode.lower() == "err":
            return ten.mean(ten.neq(predict, target))
        elif self.mode.lower() == "miss":
            return ten.sum(ten.neq(predict, target))
        else:
            raise ValueError("invalid mode")
