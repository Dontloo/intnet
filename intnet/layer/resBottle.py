from .activation import Activation
from .batchNorm import BatchNorm
from .conv2d import Conv2d
from .merge import Merge


def ResBottle(predecessor, in_dim, out_dim, name, mode=None, stride=(1, 1)):
    """
    the "bottleneck" block used in residual network https://arxiv.org/pdf/1512.03385v1.pdf
    function name capitalized to pretend that it is a class
    """
    l_1 = Activation(BatchNorm(Conv2d(predecessor, (1, 1), in_dim, name=name+'_1', no_shift=True, stride=stride)), "relu")
    l_2 = Activation(BatchNorm(Conv2d(l_1, (3, 3), in_dim, pad=1, name=name+'_2', no_shift=True)), "relu")
    l_3 = BatchNorm(Conv2d(l_2, (1, 1), out_dim, name=name+'_3', no_shift=True))
    if mode is None:
        l_in = predecessor
    elif mode == "b":
        l_in = BatchNorm(Conv2d(predecessor, (1, 1), out_dim, name=name+'_0', no_shift=True, stride=stride))
    else:
        raise ValueError("invalid mode argument")
    l_out = Activation(Merge([l_in, l_3], mode='sum'), "relu")
    return l_out
