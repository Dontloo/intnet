from .base import Layer
import theano.tensor as ten
import theano.tensor.shared_randomstreams
import theano as the


class GaussSample(Layer):
    """
    Gaussian sample layer for VAE,
    see https://github.com/fchollet/keras/blob/master/examples/variational_autoencoder.py#L27-L31
    """
    multi_pred = True

    def __init__(self, l_mean, l_log_var, **kwargs):
        Layer.__init__(self)
        # incoming layer
        self.pred = [l_mean, l_log_var]
        # dependency relations: out_shape <- param_shape <- params <- output
        # output shape
        self.out_shape = self.pred[0].out_shape
        # keyword arguments
        self.name = kwargs.get("name", None)

    def get_out(self, train):
        rng = ten.shared_randomstreams.RandomStreams()
        epsilon = rng.normal(self.out_shape, avg=0, std=1, dtype=the.config.floatX)
        return self.pred[0].out_var + ten.exp(self.pred[1].out_var / 2) * epsilon
