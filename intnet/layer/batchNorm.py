from .base import Layer
import theano as the
import numpy as np
import theano.tensor as ten


class BatchNorm(Layer):

    has_param = True
    non_grad_updates = True

    def __init__(self, predecessor, momentum=0.999, eps=1e-6, norm_axis=1, unbiased_var=1, **kwargs):
        Layer.__init__(self)
        # incoming layer
        self.pred = predecessor
        self.momentum = momentum
        self.eps = eps
        # output shape
        self.out_shape = self.pred.out_shape
        # filter-wise normalization
        self.norm_axis = norm_axis
        # use biased variance or not
        self.unbiased_var = unbiased_var
        # parameters
        self.params = {'g': the.shared(np.zeros((self.pred.out_shape[self.norm_axis],), dtype=the.config.floatX)),  # gamma
                       'b': the.shared(np.zeros((self.pred.out_shape[self.norm_axis],), dtype=the.config.floatX)),  # beta
                       'moving_mean': the.shared(np.zeros((self.pred.out_shape[self.norm_axis],), dtype=the.config.floatX)),
                       'moving_std': the.shared(np.zeros((self.pred.out_shape[self.norm_axis],), dtype=the.config.floatX))}
        # non-gradient-based updates
        self.updates = None
        # keyword arguments
        self.name = kwargs.get("name", None)

    def get_out(self, train):
        x = self.pred.out_var
        # axes for computing mean and standard deviation
        reduction_axes = range(len(self.pred.out_shape))
        del reduction_axes[self.norm_axis]
        if len(reduction_axes) == 1:
            reduction_axes = reduction_axes[0]
        # for broadcasting mean and standard deviation according to the input shapes
        shuffle_pattern = ["x" for _ in range(len(self.pred.out_shape))]
        shuffle_pattern[self.norm_axis] = 0

        if train:
            avg = ten.mean(x, axis=reduction_axes)
            # std = ten.sqrt(ten.var(x, axis=reduction_axes) + self.eps)
            std = ten.sqrt(ten.sum((x-avg[self.norm_axis])**2, axis=reduction_axes)/(ten.prod(x.shape[reduction_axes], dtype=the.config.floatX) - self.unbiased_var) + self.eps)
            # moving average is only used for estimating the mean & std to be used in testing,
            # moving average is better not be used directly for BN
            # (will slow down the training or make it impossible to train
            self.updates = [(self.params["moving_mean"], self.momentum*self.params["moving_mean"] + (1-self.momentum)*avg),
                            (self.params["moving_std"], self.momentum*self.params["moving_std"] + (1-self.momentum)*std)]
        else:
            avg = self.params["moving_mean"]
            std = self.params["moving_std"]

        brodcast_mean = avg.dimshuffle(shuffle_pattern)
        brodcast_std = std.dimshuffle(shuffle_pattern)

        # BN operation doesn't need to be disconnected from the gradient
        # brodcast_mean = the.gradient.disconnected_grad(brodcast_mean)
        # brodcast_std = the.gradient.disconnected_grad(brodcast_std)
        x_normed = (x - brodcast_mean) / brodcast_std
        return self.params["g"].dimshuffle(shuffle_pattern)*x_normed + self.params["b"].dimshuffle(shuffle_pattern)
