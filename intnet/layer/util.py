import theano as the
import theano.tensor as ten
import numpy as np


def max_acc(dist, arg_01,pair_num):
    """
    find the decision boundary that gives the maximum accuracy
    decision should follow x<threshold and x>= threshold
    this operation is disconnected from the computation graph by using theano.gradient.disconnected_grad
    """
    sort_idx = ten.argsort(dist)
    sorted_01 = arg_01[sort_idx]

    # # result is the number of mis-classifications BEFORE each index, thus has n+1 elements.
    # idx_range = ten.arange(sorted_01.shape[0] + 1)
    # num_miss, _ = the.scan(
    #     fn=lambda idx: ten.sum(sorted_01[:idx]) + sorted_01.shape[0] - idx - ten.sum(sorted_01[idx:]),
    #     sequences=idx_range)
    #
    # ---------- following is the vectorization of the above code ----------
    mask1 = the.shared(np.ones((pair_num, pair_num + 1)) - np.tri(pair_num, pair_num + 1))
    num1 = ten.dot(sorted_01, mask1)
    mask0 = the.shared(np.tri(pair_num, pair_num + 1))
    idx_vec = the.shared(np.asarray(list(reversed(range(pair_num + 1)))))
    num0 = idx_vec - ten.dot(sorted_01, mask0)

    # sum the number of ones below the threshold and the number of zeros above the threshold
    num_miss = num1 + num0

    # disconnect the output from the graph
    return the.gradient.disconnected_grad(pair_num - ten.min(num_miss))


def auto_thres(arg_01, dist, pair_num):
    """
    find the optimal threshold, based on max_acc, used for training
    decision should follow x<threshold and x>= threshold
    this operation is disconnected from the computation graph by using theano.gradient.disconnected_grad
    """
    sort_idx = ten.argsort(dist)
    sorted_01 = arg_01[sort_idx]

    # # result is the number of mis-classifications BEFORE each index, thus has n+1 elements.
    # idx_range = ten.arange(sorted_01.shape[0] + 1)
    # num_miss, _ = the.scan(
    #     fn=lambda idx: ten.sum(sorted_01[:idx]) + sorted_01.shape[0] - idx - ten.sum(sorted_01[idx:]),
    #     sequences=idx_range)
    #
    # ---------- following is the vectorization of the above code ----------
    # mask1 = the.shared(np.ones((pair_num, pair_num + 1)) - np.tri(pair_num, pair_num + 1))
    # num1 = ten.dot(sorted_01, mask1)
    # mask0 = the.shared(np.tri(pair_num, pair_num + 1))
    # idx_vec = the.shared(np.asarray(list(reversed(range(pair_num + 1)))))
    # num0 = idx_vec - ten.dot(sorted_01, mask0)
    #
    # # the n+1 element version will case index out of bound error
    # ---------- use the following n element version instead ----------
    mask1 = the.shared(np.ones(pair_num) - np.tri(pair_num))
    num1 = ten.dot(sorted_01, mask1)
    mask0 = the.shared(np.tri(pair_num))
    idx_vec = the.shared(1 + np.asarray(list(reversed(range(pair_num)))))
    num0 = idx_vec - ten.dot(sorted_01, mask0)
    # sum the number of ones below the threshold and the number of zeros above the threshold
    num_miss = num1 + num0

    boundary_idx = ten.argmin(num_miss)
    # disconnect the output from the graph
    return the.gradient.disconnected_grad(dist[boundary_idx])


def euclidean(a, b):
    return ten.sqrt(ten.sum((a - b) ** 2, axis=1))


def euclidean_sqr(a, b):
    return ten.sum((a - b) ** 2, axis=1)


def jb0cov(a, b):
    return ten.sum(a ** 2 + b ** 2 - 4 * a * b, axis=1)


def eq_sign(a, b):
    return ten.switch(ten.eq(a, b), 1, -1)


def eq_01(a, b):
    return ten.switch(ten.eq(a, b), 0, 1)
