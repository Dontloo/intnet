from .base import Layer
import intnet.com.nonLinearity as lin


class Activation(Layer):
    def __init__(self, predecessor, acfun):
        Layer.__init__(self)
        self.pred = predecessor
        self.acfun = lin.get_nonlinearity(acfun)
        # output shape
        self.out_shape = self.pred.out_shape

    def get_out(self, train):
        return self.acfun(self.pred.out_var)
