class Layer:
    """
    base class of layers
    Parameters are declared when instantiating layers,
    computation graphs are built when instantiating functions,
    such that different functions share the same parameters.
    """
    has_input = False
    has_param = False
    multi_pred = False  # multiple predecessors
    non_grad_updates = False  # update rules that don't depend on gradient information (for BN layers especially

    def __init__(self):
        # dependency relations: out_shape <- param_shape <- params <- output
        self.in_shape = None
        self.in_var = None
        self.out_shape = None
        # at compilation time, use out_var = get_out() to build the piece of computation graph specified by this layer
        # note that if get_out() is called multiple times then there'll be multiple identical paths in the graph.
        self.out_var = None
        self.pred = None
        self.params = {}
        self.name = None

    def get_out(self, train):
        """
        build computation graph.
        note that if get_out() is called multiple times then there'll be multiple identical paths in the graph.
        """
        raise NotImplementedError
