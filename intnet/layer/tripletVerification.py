from .base import Layer
import theano.tensor as ten
import theano as the
import numpy as np
from .util import *


class TripletVerification(Layer):
    """
    verification loss layer for neural networks,
    output the mean verification loss of every non-overlapping neighbouring pair,
    at most one of {arg_thres, arg_batch_sze} should be given.
    for example:
        input = [1 2 3 4 5 6]
        this layer applies a verification function to pair [1 2], [3 4] and [5 6]
    """
    multi_pred = True

    def __init__(self, predecessor, tar_layer, sim_fn, energy_fn, batch_sze, thres=None, margin=0.3, **kwargs):
        """
        pairs for verification are made of every non-overlapping neighbourhoods
        :param sim_fn & arg_energy_fn
            spring: euc
            bounded spring: euc
            bounded: jb, ecu_sqr
            same_only: jb, ecu_sqr
            hinge: jb, ecu_sqr
        """
        Layer.__init__(self)
        # predecessor
        self.pred = [predecessor, tar_layer]
        # output shape
        self.out_shape = (1,)
        # verification loss
        if sim_fn.lower() == "bounded_spring":
            self.sim_fn = self.bounded_spring
        elif sim_fn.lower() == "spring":
            self.sim_fn = self.spring
        elif sim_fn.lower() == "cluster":
            self.sim_fn = self.cluster
        elif sim_fn.lower() == "hinge":
            # parameters
            self.has_param = True
            self.params = {  # heuristic value for guessing a good initial point
                's': the.shared(np.ones(1, dtype=the.config.floatX)),
                'b': the.shared(-np.ones(1, dtype=the.config.floatX))}
            self.sim_fn = self.hinge
        else:
            raise ValueError("invalid verification similarity.")
        # verification loss
        if energy_fn.lower() == "jb":
            self.energy_fn = jb0cov
        elif energy_fn.lower() == "euc":
            self.energy_fn = euclidean
        elif energy_fn.lower() == "euc_sqr":
            self.energy_fn = euclidean_sqr
        else:
            raise ValueError("invalid verification energy.")
        # threshold parameter for loss function
        self.thres = thres
        # margin parameter for (some) loss functions
        self.margin = margin
        # batch size (in order to accelerate finding the optimal threshold when the thres parameter is not provided
        if batch_sze % 3 != 0:
            raise ValueError("batch size muse be a multiple of 3")
        self.triplet_num = batch_sze / 3
        # self.label_01 = the.shared(np.concatenate((np.zeros((self.triplet_num,)), np.ones((self.triplet_num,)))))
        # keyword arguments
        self.name = kwargs.get("name", None)

    def get_out(self, train):
        x = self.pred[0].out_var[::3]
        p = self.pred[0].out_var[1::3]
        n = self.pred[0].out_var[2::3]
        p_dist = self.energy_fn(x, p)
        n_dist = self.energy_fn(x, n)
        return self.sim_fn(p_dist, n_dist)

    # same as bounded
    # TODO: to be tested
    # use euclidean distance for hinge loss directly will cause the gradients to blow up
    # but the squared version or jb0cov are smooth ones
    # http://www.wolframalpha.com/input/?i=%28x-y%29%5E2, http://www.wolframalpha.com/input/?i=x%5E2%2By%5E2-4*x*y
    def hinge(self, p_dist, n_dist):
        if self.thres is None:
            label_01 = ten.as_tensor_variable(
                np.concatenate((np.zeros((self.triplet_num,)), np.ones((self.triplet_num,)))))
            self.thres = auto_thres(label_01, ten.concatenate([p_dist, n_dist]), 2 * self.triplet_num)
        loss = ten.maximum(0, self.margin + p_dist - self.thres) + ten.maximum(0, self.margin - n_dist + self.thres)
        return ten.mean(loss)

    def cluster(self, p_dist, n_dist):
        loss = ten.maximum(0, p_dist - n_dist + 2)
        return ten.mean(loss)

    def combine(self, p_dist, n_dist):
        loss = ten.maximum(0, ten.sqr(p_dist) - ten.sqr(n_dist) + self.margin + self.thres)
        return ten.mean(loss) / 2

    # spring model with euclidean distance as energy function
    # Dimensionality Reduction by Learning an Invariant Mapping, Raia Hadsell, Sumit Chopra, Yann LeCun CVPR 2006
    # http://yann.lecun.com/exdb/publis/pdf/hadsell-chopra-lecun-06.pdf
    def spring(self, p_dist, n_dist):
        if self.thres is None:
            label_01 = ten.as_tensor_variable(
                np.concatenate((np.zeros((self.triplet_num,)), np.ones((self.triplet_num,)))))
            self.thres = auto_thres(label_01, ten.concatenate([p_dist, n_dist]), 2 * self.triplet_num)
        # if eq: 0.5*dist^2
        # if neq: max(0, (m-dist)^2)
        # max(0,m-d)^2 is not equivalent to max(0,m-d^2)
        # 1) decrease fast at first, 2) decrease slow at first
        loss = ten.concatenate([ten.sqr(p_dist), ten.sqr(ten.maximum(0, self.margin + self.thres - n_dist))])
        # loss = ten.sqr(p_dist)
        return ten.mean(loss) / 2

    def bounded_spring(self, p_dist, n_dist):
        if self.thres is None:
            label_01 = ten.as_tensor_variable(
                np.concatenate((np.zeros((self.triplet_num,)), np.ones((self.triplet_num,)))))
            self.thres = auto_thres(label_01, ten.concatenate([p_dist, n_dist]), 2 * self.triplet_num)
        # if we write like this: ten.sqr(ten.minimum(self.thres, dist)),
        # theano will give nan. -_-
        loss = ten.minimum(4 * self.thres ** 2, ten.sqr(p_dist)) + ten.sqr(ten.maximum(0, self.thres - n_dist))
        return ten.mean(loss) / 2
