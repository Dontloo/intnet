from .base import Layer
from theano.tensor.nnet import conv
import theano.tensor as ten
import theano as the
import numpy as np
import intnet.com.nonLinearity as lin


class LocalConv2d(Layer):
    """
    locally connected convolutional layer,
    weights can be shared within non-overlapping regions by setting the arg_region_shape parameter.
    """
    has_param = True

    def __init__(self, predecessor, kernel_shape, filter_num, region_shape=(1, 1), no_shift=False, **kwargs):
        Layer.__init__(self)
        # predecessor
        self.pred = predecessor
        # kernel shape
        self.kernel_shape = kernel_shape
        # number of filters
        self.filter_num = filter_num
        # shape of non-overlapping regions in the output that shares the same weights
        self.region_shape = region_shape
        # patch shape
        self.patch_shape = self.get_patch_shape()

        # dependency relations: out_shape <- param_shape <- params <- output
        # output shape
        self.out_shape = self.get_out_shape()
        # parameter number
        self.param_num = self.get_param_num()
        # parameters
        self.params = {'w': the.shared(np.zeros(self.get_w_shape(), dtype=the.config.floatX))}
        # whether to have the shift parameter b, as it's no need if say batch normalization is applied
        if not no_shift:
            self.params["b"] = the.shared(np.zeros(self.param_num + (self.filter_num,), dtype=the.config.floatX))
        # keyword arguments
        self.name = kwargs.get("name", None)

    def get_param_num(self):
        """
        number of parameters, which equals the number of non-overlapping regions
        """
        param_row = self.out_shape[2] // self.region_shape[0]
        param_col = self.out_shape[3] // self.region_shape[1]
        return param_row, param_col

    def get_out(self, train):
        # TODO: validation required
        in_var = self.pred.out_var
        # perform convolution by patches and concatenate results together
        r_lst = []
        for r in range(self.param_num[0]):
            c_list = []
            for c in range(self.param_num[1]):
                l0 = r * self.region_shape[0]
                l1 = c * self.region_shape[1]
                ih0 = l0 + self.region_shape[0] + self.kernel_shape[0] - 1
                ih1 = l1 + self.region_shape[1] + self.kernel_shape[1] - 1
                sub_ten = conv.conv2d(input=in_var[:, :, l0:ih0, l1:ih1],
                                      filters=self.params["w"][r, c],
                                      image_shape=self.patch_shape,
                                      filter_shape=self.params["w"].get_value().shape[2:],
                                      border_mode='valid')
                if "b" in self.params:
                    sub_ten += self.params["b"][r, c].dimshuffle('x', 0, 'x', 'x')
                # the ten.set_subtensor operation seems to be slow, costly and error-prone
                # use concatenate instead
                c_list.append(sub_ten)
            r_lst.append(ten.concatenate(c_list, axis=3))
        tmp = ten.concatenate(r_lst, axis=2)
        return tmp

    def get_patch_shape(self):
        """
        shape of patches taken from the input to perform a convolution
        """
        return self.pred.out_shape[:2] + \
            (self.region_shape[0] + self.kernel_shape[0] - 1, self.region_shape[1] + self.kernel_shape[1] - 1)

    def get_w_shape(self):
        return self.param_num + (self.filter_num, self.pred.out_shape[1]) + self.kernel_shape

    def get_out_shape(self):
        def get1d(in_len, kernel_len):
            out_len = (in_len - kernel_len + 1)
            return out_len

        out_row = get1d(self.pred.out_shape[2], self.kernel_shape[0])
        out_col = get1d(self.pred.out_shape[3], self.kernel_shape[1])

        return self.pred.out_shape[0], self.filter_num, out_row, out_col
