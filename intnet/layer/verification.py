from .base import Layer
import theano.tensor as ten
import theano as the
import numpy as np
from .util import *


class Verification(Layer):
    """
    verification loss layer for neural networks,
    output the mean verification loss of every non-overlapping neighbouring pair,
    at most one of {arg_thres, arg_batch_sze} should be given.
    for example:
        input = [1 2 3 4 5 6]
        this layer applies a verification function to pair [1 2], [3 4] and [5 6]
    """
    multi_pred = True

    def __init__(self, predecessor, tar_layer, sim_fn, energy_fn, batch_sze, thres=None, margin=0, **kwargs):
        """
        pairs for verification are made of every non-overlapping neighbourhoods
        :param sim_fn & arg_energy_fn
            spring: euc
            bounded spring: euc
            bounded: jb, ecu_sqr
            same_only: jb, ecu_sqr
            sigmoid: jb, ecu_sqr (not recommended
        """
        Layer.__init__(self)
        # predecessor
        self.pred = [predecessor, tar_layer]
        # output shape
        self.out_shape = (1,)
        # verification loss
        if sim_fn.lower() == "bounded_spring":
            self.sim_fn = self.bounded_spring
        elif sim_fn.lower() == "spring":
            self.sim_fn = self.spring
        elif sim_fn.lower() == "hinge":
            self.sim_fn = self.hinge
        elif sim_fn.lower() == "bounded":
            self.sim_fn = self.bounded
        elif sim_fn.lower() == "sigmoid":
            # parameters
            self.has_param = True
            self.params = {  # heuristic value for guessing a good initial point
                's': the.shared(0.02*np.ones(1, dtype=the.config.floatX)),
                'b': the.shared(6*np.ones(1, dtype=the.config.floatX))}
            self.sim_fn = self.sigmoid
        else:
            raise ValueError("invalid verification similarity.")
        # verification loss
        if energy_fn.lower() == "jb":
            self.energy_fn = jb0cov
        elif energy_fn.lower() == "euc":
            self.energy_fn = euclidean
        elif energy_fn.lower() == "euc_sqr":
            self.energy_fn = euclidean_sqr
        else:
            raise ValueError("invalid verification energy.")
        # threshold parameter for loss function
        self.thres = thres
        # margin parameter for (some) loss functions
        self.margin = margin
        # batch size (in order to accelerate finding the optimal threshold when the thres parameter is not provided
        if batch_sze%2 != 0:
            raise ValueError("batch size muse be a even number")
        self.pair_num = batch_sze/2
        # keyword arguments
        self.name = kwargs.get("name", None)

    def get_out(self, train):
        odd_x = self.pred[0].out_var[::2]
        even_x = self.pred[0].out_var[1::2]
        odd_y = self.pred[1].out_var[::2]
        even_y = self.pred[1].out_var[1::2]
        dist = self.energy_fn(odd_x, even_x)
        return self.sim_fn(dist, odd_y, even_y)

    def hinge(self, dist, y1, y2):
        if self.thres is None:
            self.thres = auto_thres(eq_01(y1, y2), dist, self.pair_num)
        loss = ten.maximum(0, self.margin+eq_sign(y1, y2)*(dist-self.thres))
        return ten.mean(loss)

    def same_only(self, dist, y1, y2):
        if self.thres is None:
            loss = ten.switch(ten.eq(y1, y2), dist, 0) / 2
        else:
            loss = ten.switch(ten.eq(y1, y2), ten.minimum(ten.maximum(dist, -self.thres), self.thres), 0)
        return ten.mean(loss)/2

    # parameters of sigmoid don't get updated,
    # plausibly because d_E/d_sig is too small,
    # hard to find a good initial point.
    def sigmoid(self, dist, y1, y2):
        p = ten.nnet.sigmoid(self.params['s'].dimshuffle(0, 'x')*dist - self.params['b'].dimshuffle(0, 'x'))
        loss = ten.sqr(eq_01(y1, y2) - p)
        return ten.mean(loss)/2

    # spring model with euclidean distance as energy function
    # Dimensionality Reduction by Learning an Invariant Mapping, Raia Hadsell, Sumit Chopra, Yann LeCun CVPR 2006
    # http://yann.lecun.com/exdb/publis/pdf/hadsell-chopra-lecun-06.pdf
    def spring(self, dist, y1, y2):
        if self.thres is None:
            self.thres = auto_thres(eq_01(y1, y2), dist, self.pair_num)
        # if eq: 0.5*dist^2
        # if neq: max(0, (m-dist)^2)
        loss = ten.switch(ten.eq(y1, y2),
                          ten.sqr(dist),
                          # max(0,m-d)^2 is not equivalent to max(0,m-d^2)
                          # 1) decrease fast at first, 2) decrease slow at first
                          ten.sqr(ten.maximum(0, self.margin + self.thres - dist)))
        return ten.mean(loss)/2

    def bounded_spring(self, dist, y1, y2):
        if self.thres is None:
            self.thres = auto_thres(eq_01(y1, y2), dist, self.pair_num)
        loss = ten.switch(ten.eq(y1, y2),
                          # if we write like this: ten.sqr(ten.minimum(self.thres, dist)),
                          # theano will give nan. -_-
                          ten.maximum((0.5*self.thres)**2, ten.minimum((2*self.thres)**2, ten.sqr(dist))),
                          ten.sqr(ten.maximum(0, self.thres - dist)))
        return ten.mean(loss)/2

    # use euclidean distance with hinge loss directly will cause the gradients to blow up
    # but the squared version or jb0cov are smooth ones
    # http://www.wolframalpha.com/input/?i=%28x-y%29%5E2, http://www.wolframalpha.com/input/?i=x%5E2%2By%5E2-4*x*y
    def bounded(self, dist, y1, y2):
        if self.thres is None:
            self.thres = auto_thres(eq_01(y1, y2), dist, self.pair_num)
        loss = eq_sign(y1, y2) * ten.minimum(ten.maximum(dist, -self.thres), self.thres)
        return ten.mean(loss)/2
