from .base import Layer
import theano.tensor as ten


class Merge(Layer):
    """
    linear combination
    """
    multi_pred = True

    def __init__(self, predecessors, mode, coefficients=None, **kwargs):
        Layer.__init__(self)
        if mode not in {"sum", "lin_comb", "concatenate"}:
            raise ValueError("unsupported mode")
        self.mode = mode
        # incoming layer
        self.pred = predecessors
        # output shape
        self.out_shape = self.get_out_shape()
        # coefficients
        self.coefficients = coefficients
        # keyword arguments
        self.name = kwargs.get("name", None)

    def get_out_shape(self):
        if self.mode in {"sum", "lin_comb"}:
            if all(p.out_shape == self.pred[0].out_shape for p in self.pred):
                return self.pred[0].out_shape
            else:
                print [p.out_shape for p in self.pred]
                raise ValueError("dimensions must agree")
        elif self.mode == "concatenate":
            # TODO: shape check
            return self.pred[0].out_shape[0], sum(p.out_shape[1] for p in self.pred)

    def get_out(self, train):
        if self.mode == "lin_comb":
            return sum(map(lambda x, y: x*y.out_var, self.coefficients, self.pred))
        elif self.mode == "sum":
            return sum(p.out_var for p in self.pred)
        elif self.mode == "concatenate":
            return ten.concatenate([p.out_var for p in self.pred], axis=1)
