import theano as the
import theano.tensor as ten
import intnet
import pickle
import numpy as np
import timeit


class JointBayesian:
    def __init__(self, feature_fn, feature_dim, feature_mean=0):
        self.feature_fn = feature_fn
        self.feature_dim = feature_dim
        # if EM training is not employed,
        # use the A and G correspond to zero covariances
        self.A = -np.eye(self.feature_dim)
        self.G = -2 * np.eye(self.feature_dim)
        # and zero mean
        self.feature_mean = feature_mean

    def joint_bayesian_ten(self, x1, x2):
        x1 -= self.feature_mean
        x2 -= self.feature_mean
        # the return value is equivalent to:
        # -(ten.diagonal(ten.dot(ten.dot(x1, A), x1.T))
        # + ten.diagonal(ten.dot(ten.dot(x2, A), x2.T))
        # - 2 * ten.diagonal(ten.dot(ten.dot(x1, G), x2.T)))
        return -(ten.sum(ten.dot(x1, self.A) * x1, axis=1) +
                 ten.sum(ten.dot(x2, self.A) * x2, axis=1) -
                 2 * ten.sum(ten.dot(x1, self.G) * x2, axis=1))

    def joint_bayesian(self, x1, x2):
        x1 -= self.feature_mean
        x2 -= self.feature_mean
        return np.dot(np.dot(x1, self.A), x1) + np.dot(np.dot(x2, self.A), x2) - 2 * np.dot(np.dot(x1, self.G), x2)

    def joint_bayesian_EM(self, train_dat, iter=100, thres=1e-3, cov_ini="eye", inv_fn=np.linalg.pinv):
        """
        if called with iter=0, will only set the feature mean, and do initialization
        """
        # prepare data
        cate_lst = []
        len_lst = []
        for x, y in intnet.data.CategoryLauncher(train_dat):
            cate_lst.append(self.feature_fn(x) - self.feature_mean)
            len_lst.append(len(y))

        buff_size = max(len_lst) + 1
        buff_msk = np.zeros(buff_size)
        buff_msk[len_lst] = 1

        # initialize covariance matrix
        S_mu, S_eps = self.__init_cov(self.feature_dim, cov_ini, cate_lst, len_lst)

        S_mu_FmG = np.zeros((buff_size, self.feature_dim, self.feature_dim))
        S_eps_G = np.zeros((buff_size, self.feature_dim, self.feature_dim))

        for z in range(iter):
            print "epoch", z
            F = inv_fn(S_eps)
            for m in range(buff_size):
                if buff_msk[m] == 1:
                    G = -np.dot(np.dot(inv_fn(m * S_mu + S_eps), S_mu), inv_fn(S_eps))
                    S_mu_FmG[m] = np.dot(S_mu, F + m * G)
                    S_eps_G[m] = np.dot(S_eps, G)

            mu = []
            eps = []
            for i in range(len(cate_lst)):
                m = len_lst[i]
                sum_x = np.sum(cate_lst[i], 0)
                mu.append(np.dot(S_mu_FmG[m], sum_x))
                eps.append(cate_lst[i] + np.dot(S_eps_G[m], sum_x))

            old_S_mu = S_mu
            old_S_eps = S_eps
            S_mu = np.cov(np.vstack(mu).T, ddof=1)
            S_eps = np.cov(np.vstack(eps).T, ddof=len(len_lst))

            delta_S_mu = np.linalg.norm(S_mu - old_S_mu) / np.linalg.norm(S_mu)
            delta_S_eps = np.linalg.norm(S_eps - old_S_eps) / np.linalg.norm(S_eps)
            print delta_S_mu, delta_S_eps
            if delta_S_mu <= thres and delta_S_eps <= thres:
                break

        F = inv_fn(S_eps)
        self.G = -np.dot(np.dot(inv_fn(2 * S_mu + S_eps), S_mu), inv_fn(S_eps))
        self.A = inv_fn(S_mu + S_eps) - (F + self.G)

    def __init_cov(self, data_dim, method, train_data, train_data_len):
        if method.lower() == "random":
            return np.cov(np.random.rand(data_dim, data_dim)), np.cov(np.random.rand(data_dim, data_dim))
        elif method.lower() == "eye":
            return np.eye(data_dim), np.eye(data_dim)
        elif method.lower() == "lda":
            S_mu = np.zeros((data_dim, data_dim))
            S_eps = np.zeros((data_dim, data_dim))
            data_num = sum(train_data_len)
            cate_num = len(train_data_len)
            for i in range(len(train_data_len)):
                m_k = np.mean(train_data[i], 0)
                S_mu += np.outer(m_k, m_k)
                # if a category contains only one point, tmp would be 0
                tmp = train_data[i] - m_k
                S_eps += np.dot(tmp.T, tmp)

            S_mu /= cate_num - 1
            S_eps /= data_num - cate_num
            return S_mu, S_eps
        else:
            raise ValueError("invalid covariance initialization.")

    def veri(self, pair_lst):
        """
        verification using a trained deepid model
        """

        # building verification function
        l_x1 = intnet.layer.Input((None, None), the.config.floatX)
        l_x2 = intnet.layer.Input((None, None), the.config.floatX)
        l_y = intnet.layer.Input((None,), "int32")
        l_jb = intnet.jointBayesian.Verification(l_x1, l_x2, l_y, self.joint_bayesian_ten, pair_num=len(pair_lst))

        self.veri_net = intnet.ffnn.ModelFFNN(l_jb)
        veri_fn = self.veri_net.output_fn([l_x1, l_x2, l_y], l_jb)

        pair_ds = intnet.data.PairMatchData(pair_lst, "on_drive", x_dtype=the.config.floatX)

        batch_sze = 300
        f1 = []
        f2 = []
        y_lst = []
        for i, x1, x2, y in intnet.data.EpochBatchLauncher(pair_ds, batch_sze):
            f1.extend(self.feature_fn(x1))
            f2.extend(self.feature_fn(x2))
            y_lst.extend(y)
        count = veri_fn(f1, f2, y_lst)

        print count, "/", len(pair_lst), count / len(pair_lst)
        return count / len(pair_lst)


# if __name__ == "__main__":
#     pair_lst_file = "../../../../data/LFW/files/pair_webNorm_37*37.pickle"
#     print "reading testing pair list for verification."
#     with open(pair_lst_file, 'rb') as f:
#         pickle.load(f)
#         pair_lst = pickle.load(f)
#
    # jb = JointBayesian(deep_net.feature_fn, 160)
#     jb.veri(pair_lst)
#     jb.joint_bayesian_EM(train_lst, cov_ini="eye", iter=0, thres=1e-4)
#     jb.veri(pair_lst)
#     jb.joint_bayesian_EM(train_lst, cov_ini="lda", iter=0, thres=1e-4)
#     jb.veri(pair_lst)
