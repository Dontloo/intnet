import theano.tensor as ten
import theano as the
import numpy as np
import theano.typed_list


# not completed, need to be reorganized, but barely works
class JointBayesianTheano():
    has_input = True

    def __init__(self, cate_num, feature_dim):
        self.cate_num = cate_num
        self.feature_dim = feature_dim
        # input variable
        self.cate_mat = theano.typed_list.TypedListType(ten.TensorType(the.config.floatX, broadcastable=(None, None)))()
        # input shape
        self.cate_len = ten.vector(dtype=the.config.floatX)
        self.in_shape = (cate_num, None, feature_dim)
        # output shape
        self.out_shape = (feature_dim, feature_dim)
        # parameters
        self.S_mu = None
        self.S_eps = None

    def em(self, cate_mat_val, cate_len_val, cov_ini):
        # ---------------------- initialize covariance ----------------------
        if cov_ini.lower() == "lda":

            def lda_fn(i, L, mu, eps):
                m_k = ten.mean(L[i], 0)
                tmp = L[i] - m_k
                return {mu: mu+ten.outer(m_k, m_k), eps: eps+ten.dot(tmp.T, tmp)}

            self.S_mu = the.shared(np.zeros((self.feature_dim, self.feature_dim), dtype=the.config.floatX))
            self.S_eps = the.shared(np.zeros((self.feature_dim, self.feature_dim), dtype=the.config.floatX))
            result, updates = the.scan(fn=lda_fn,
                                       sequences=[the.tensor.arange(self.cate_num, dtype='int64')],
                                       non_sequences=[self.cate_mat, self.S_mu, self.S_eps])
            init_fn = the.function([self.cate_mat], updates=updates)
            init_fn(cate_mat_val)
            self.S_mu /= self.cate_num
            self.S_eps /= ten.sum(self.cate_len)
        elif cov_ini.lower() == "eye":
            self.S_mu = the.shared(np.eye(self.feature_dim, dtype=the.config.floatX))
            self.S_eps = the.shared(np.eye(self.feature_dim, dtype=the.config.floatX))
        else:  # random
            self.S_mu = the.shared(np.cov(np.random.rand(self.feature_dim, self.feature_dim)).astype(the.config.floatX))
            self.S_eps = the.shared(np.cov(np.random.rand(self.feature_dim, self.feature_dim)).astype(the.config.floatX))
        # ---------------------- compute A&G ----------------------
        inv_fn = ten.nlinalg.MatrixPinv()
        F = inv_fn(self.S_eps)
        G = -ten.dot(ten.dot(inv_fn(2 * self.S_mu + self.S_eps), self.S_mu), inv_fn(self.S_eps))
        A = inv_fn(self.S_mu + self.S_eps) - (F + G)
        em_fn = the.function([self.cate_mat, self.cate_len], [A, G], on_unused_input='warn')
        return em_fn(cate_mat_val, cate_len_val)

    def dist_fn(self):
        A = ten.matrix(dtype=the.config.floatX)
        G = ten.matrix(dtype=the.config.floatX)
        x1 = ten.matrix(dtype=the.config.floatX)
        x2 = ten.matrix(dtype=the.config.floatX)
        dist = -(ten.sum(ten.dot(x1, A)*x1, axis=1) +
                ten.sum(ten.dot(x2, A)*x2, axis=1) -
                2*ten.sum(ten.dot(x1, G)*x2, axis=1))
        return the.function([x1, x2, A, G], dist)
