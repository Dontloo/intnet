import intnet
from intnet.layer.util import *


class Verification(intnet.layer.Layer):
    """
    verification layer for joint Bayesian
    """
    multi_pred = True
    has_param = False

    def __init__(self, x1_layer, x2_layer, y_layer, dist_fn, pair_num, **kwargs):
        """
        :param y_layer: 0 for same 1 for different
        """
        intnet.layer.Layer.__init__(self)
        # predecessor
        self.pred = [x1_layer, x2_layer, y_layer]
        # output shape
        self.out_shape = (1,)
        # number of pairs
        self.pair_num = pair_num

        if hasattr(dist_fn, '__call__'):
            self.dist_fn = dist_fn
        elif dist_fn.lower() == "jb0cov":
            self.dist_fn = jb0cov
        elif dist_fn.lower() == "euc":
            self.dist_fn = euclidean
        else:
            raise ValueError("invalid verification distance function.")

    def get_out(self, train):
        dist = self.dist_fn(self.pred[0].out_var, self.pred[1].out_var)
        return max_acc(dist, self.pred[2].out_var, self.pair_num)
