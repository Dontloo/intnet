from .sgd import SGDOptimizer
from .momentum import MomentumOptimizer
from .nesterov import NesterovOptimizer
from .adagrad import AdaGradOptimizer
from .adam import AdamOptimizer
from .adamax import AdaMaxOptimizer
from .myada import MyAdaOptimizer
from .rmsprop import RMSPROPOptimizer


def get_optimizer(arg_method, *args):
    if arg_method.lower() == "sgd":
        return SGDOptimizer(*args)
    elif arg_method.lower() == "momentum":
        return MomentumOptimizer(*args)
    elif arg_method.lower() == "nesterov":
        return NesterovOptimizer(*args)
    elif arg_method.lower() == "adagrad":
        return AdaGradOptimizer(*args)
    elif arg_method.lower() == "adam":
        return AdamOptimizer(*args)
    elif arg_method.lower() == "adamax":
        return AdaMaxOptimizer(*args)
    elif arg_method.lower() == "my_ada":
        return MyAdaOptimizer(*args)
    elif arg_method.lower() == "rmsprop":
        return RMSPROPOptimizer(*args)
    else:
        raise ValueError("no corresponding initializer.")
