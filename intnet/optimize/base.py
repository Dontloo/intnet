class Optimizer:
    """
    an optimizer instance has its own parameters,
    use different instances for different training processes,
    otherwise the parameters will be shared.
    """
    def __init__(self):
        self.loss = None
        self.opt_param = []  # parameters of the optimizer

    def set_loss(self, arg_loss):
        self.loss = arg_loss

    def param_updates(self, param_lst):
        """
        initialize all optimizer parameters
        """
        raise NotImplementedError

    def update_learning_params(self, cur_iter, max_iter):
        raise NotImplementedError

    def set_optimizer_param(self, snapshot):
        """
        may need to reset the shared parameters (if there're) before starting training
        otherwise the shared parameters in the latest training task will be used
        """
        raise NotImplementedError
