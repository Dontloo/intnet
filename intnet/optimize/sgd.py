from .base import Optimizer
import theano as the
import numpy as np
from collections import OrderedDict


class SGDOptimizer(Optimizer):
    def __init__(self, adapt_lr):
        Optimizer.__init__(self)
        self.adapt_lr = adapt_lr
        self.tensor_lr = the.shared(np.float32(0))

    def param_updates(self, param_lst):
        updates = OrderedDict()
        for p in param_lst:
            self.update_a_param(p, updates)
        return updates

    def update_a_param(self, var, updates):
        grad = the.grad(self.loss, var, disconnected_inputs='warn')
        # update
        updates[var] = var - self.tensor_lr * grad

    def update_learning_params(self, cur_iter, max_iter):
        self.tensor_lr.set_value(self.adapt_lr.get_val(cur_iter, max_iter))

    def set_optimizer_param(self, snapshot):
        pass
