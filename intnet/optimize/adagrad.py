from .base import Optimizer
import theano as the
import numpy as np
from collections import OrderedDict
import theano.tensor as ten


class AdaGradOptimizer(Optimizer):
    """
    moentum + adagrad, without decay in G
    """
    def __init__(self, adapt_lr, init_accum=0.01):
        Optimizer.__init__(self)
        self.adapt_lr = adapt_lr
        self.tensor_lr = the.shared(np.float32(0))

        self.init_accum = init_accum
        # optimizer parameters
        self.accum_lst = []

    def param_updates(self, param_lst):
        updates = OrderedDict()
        for p in param_lst:
            grad = the.grad(self.loss, p, disconnected_inputs='warn')
            value = p.get_value(borrow=True)

            accum = the.shared(self.init_accum*np.ones(value.shape, dtype=value.dtype), broadcastable=p.broadcastable)
            self.accum_lst.append(accum)

            self.update_a_param(p, accum, grad, updates)
        return updates

    def update_a_param(self, param, accum, grad, updates):
        # adagrad: http://www.magicbroom.info/Papers/DuchiHaSi10.pdf
        # accum += grad^2
        # var -= lr*grad*(1/sqrt(accum))
        # for this diagonalized version, there's no need for the smoothing term to avoid division by zero,
        # because accum will always be positive and increasing.
        new_a = accum + grad**2
        new_p = param - self.tensor_lr*grad*(1/ten.sqrt(new_a))
        # update
        updates[accum] = new_a
        updates[param] = new_p

    def update_learning_params(self, cur_iter, max_iter):
        self.tensor_lr.set_value(self.adapt_lr.get_val(cur_iter, max_iter))

    def set_optimizer_param(self, snapshot):
        if snapshot is None or snapshot.lower() == 'init':
            for accum in self.accum_lst:
                value = accum.get_value(borrow=True)
                accum.set_value(self.init_accum*np.ones(value.shape, dtype=value.dtype))
