from .base import Optimizer
import theano as the
import numpy as np
from collections import OrderedDict
import theano.tensor as ten


class AdaMaxOptimizer(Optimizer):
    def __init__(self, adapt_lr, adapt_mom, init_accum=0.001, decay_rate=0.999, eps=1e-8):
        Optimizer.__init__(self)
        self.adapt_lr = adapt_lr
        self.tensor_lr = the.shared(np.float32(0))
        self.adapt_mom = adapt_mom
        self.tensor_mom = the.shared(np.float32(0))

        self.init_accum = init_accum
        self.decay_rate = decay_rate
        self.eps = eps
        # optimizer parameters
        self.velocity_lst = []
        self.accum_lst = []

    def param_updates(self, param_lst):
        updates = OrderedDict()
        for p in param_lst:
            grad = the.grad(self.loss, p, disconnected_inputs='warn')
            value = p.get_value(borrow=True)

            velocity = the.shared(np.zeros(value.shape, dtype=value.dtype), broadcastable=p.broadcastable)
            accum = the.shared(self.init_accum*np.ones(value.shape, dtype=value.dtype), broadcastable=p.broadcastable)
            self.velocity_lst.append(velocity)
            self.accum_lst.append(accum)

            self.update_a_param(p, velocity, accum, grad, updates)
        return updates

    def update_a_param(self, param, velocity, accum, grad, updates):
        # cs231: http://cs231n.github.io/neural-networks-3/
        # [Adam: A Method for Stochastic Optimization] Dec 2014
        new_v = self.tensor_mom*velocity + (1-self.tensor_mom)*grad
        new_a = ten.maximum(self.decay_rate*accum, abs(grad))
        new_p = param - self.tensor_lr*new_v*(1/(new_a+self.eps))
        # update
        updates[param] = new_p
        updates[accum] = new_a
        updates[velocity] = new_v

    def update_learning_params(self, cur_iter, max_iter):
        self.tensor_lr.set_value(self.adapt_lr.get_val(cur_iter, max_iter))
        self.tensor_mom.set_value(self.adapt_mom.get_val(cur_iter, max_iter))

    def set_optimizer_param(self, snapshot):
        if snapshot is None or snapshot.lower() == 'init':
            for velocity in self.velocity_lst:
                value = velocity.get_value(borrow=True)
                velocity.set_value(np.zeros(value.shape, dtype=value.dtype))
            for accum in self.accum_lst:
                value = accum.get_value(borrow=True)
                accum.set_value(self.init_accum*np.ones(value.shape, dtype=value.dtype))
