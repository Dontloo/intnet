from .base import Optimizer
import theano as the
import numpy as np
import theano.tensor as ten
from collections import OrderedDict


class RMSPROPOptimizer(Optimizer):
    def __init__(self, adapt_lr, decay_rate=0.9, init_accum=0.1, eps=1e-6):
        Optimizer.__init__(self)
        self.decay_rate = decay_rate
        self.init_accum = init_accum
        self.eps = eps

        self.adapt_lr = adapt_lr
        self.tensor_lr = the.shared(np.float32(0))
        # optimizer parameters
        self.accum_lst = []

    def param_updates(self, param_lst):
        updates = OrderedDict()
        for p in param_lst:
            grad = the.grad(self.loss, p, disconnected_inputs='warn')
            value = p.get_value(borrow=True)

            accum = the.shared(self.init_accum*np.ones(value.shape, dtype=value.dtype), broadcastable=p.broadcastable)
            self.accum_lst.append(accum)

            self.update_a_param(p, accum, grad, updates)
        return updates

    def update_a_param(self, param, accum, grad, updates):
        # cs231: http://cs231n.github.io/neural-networks-3/
        # applying the (1-decay_rate) factor or not does not matter very much,
        # as it just moves accum to another equilibrium point.
        new_a = self.decay_rate*accum + (1-self.decay_rate)*grad**2
        nwe_p = param - self.tensor_lr*grad*(1/(ten.sqrt(new_a)+self.eps))
        # update
        updates[accum] = new_a
        updates[param] = nwe_p

    def update_learning_params(self, cur_iter, max_iter):
        self.tensor_lr.set_value(self.adapt_lr.get_val(cur_iter, max_iter))

    def set_optimizer_param(self, snapshot):
        if snapshot is None or snapshot.lower() == 'init':
            for accum in self.accum_lst:
                value = accum.get_value(borrow=True)
                accum.set_value(self.init_accum*np.ones(value.shape, dtype=value.dtype))
