from .base import Optimizer
import theano as the
import numpy as np
from collections import OrderedDict


class NesterovOptimizer(Optimizer):
    def __init__(self, adapt_lr, adapt_mom):
        Optimizer.__init__(self)
        self.adapt_lr = adapt_lr
        self.tensor_lr = the.shared(np.float32(0))
        self.adapt_mom = adapt_mom
        self.tensor_mom = the.shared(np.float32(0))
        # optimizer parameters
        self.velocity_lst = []

    def param_updates(self, param_lst):
        updates = OrderedDict()
        for p in param_lst:
            grad = the.grad(self.loss, p, disconnected_inputs='warn')
            value = p.get_value(borrow=True)

            velocity = the.shared(np.zeros(value.shape, dtype=value.dtype), broadcastable=p.broadcastable)
            self.velocity_lst.append(velocity)

            self.update_a_param(p, velocity, grad, updates)
        return updates

    def update_a_param(self, param, velocity, grad, updates):
        # Nesterov momentum
        # new_p = p - lr*g + m*(m*v - lr*g)
        # ref:
        # Hinton's video "a bag of tricks for mini-batch gradient descent"
        # CS231: http://cs231n.github.io/neural-networks-3/#sgd
        # cross validated: http://stats.stackexchange.com/a/191727/95569
        # lasagne: https://github.com/lisa-lab/pylearn2/pull/136#issuecomment-1038161
        # changing momentum over time wouldn't cause deviation,
        # because the step 0->2 does not involve momentum,
        # it only changes the step of gambling as it's supposed to do.

        # Nesterov method gives more weight to the lr*g term, and less weight to the v term, and nothing else.

        # can not write like
        # up[v] = m*v - lr*g
        # up[x] = x - lr*g + m*up[v]
        # because Theano won't update up[v] until the next function call
        new_v = self.tensor_mom*velocity - self.tensor_lr*grad
        new_p = param - self.tensor_lr*grad + self.tensor_mom*new_v
        # update
        updates[velocity] = new_v
        updates[param] = new_p

    def update_learning_params(self, cur_iter, max_iter):
        self.tensor_lr.set_value(self.adapt_lr.get_val(cur_iter, max_iter))
        self.tensor_mom.set_value(self.adapt_mom.get_val(cur_iter, max_iter))

    def set_optimizer_param(self, snapshot):
        if snapshot is None or snapshot.lower() == 'init':
            for velocity in self.velocity_lst:
                value = velocity.get_value(borrow=True)
                velocity.set_value(np.zeros(value.shape, dtype=value.dtype))
