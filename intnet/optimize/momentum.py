from .base import Optimizer
import theano as the
import numpy as np
from collections import OrderedDict


class MomentumOptimizer(Optimizer):
    def __init__(self, adapt_lr, adapt_mom):
        Optimizer.__init__(self)
        self.adapt_lr = adapt_lr
        self.tensor_lr = the.shared(np.float32(0))
        self.adapt_mom = adapt_mom
        self.tensor_mom = the.shared(np.float32(0))
        # optimizer parameters
        self.velocity_lst = []

    def param_updates(self, param_lst):
        updates = OrderedDict()
        for p in param_lst:
            grad = the.grad(self.loss, p, disconnected_inputs='warn')
            value = p.get_value(borrow=True)

            velocity = the.shared(np.zeros(value.shape, dtype=value.dtype), broadcastable=p.broadcastable)
            self.velocity_lst.append(velocity)

            self.update_a_param(p, velocity, grad, updates)
        return updates

    def update_a_param(self, param, velocity, grad, updates):
        # the name should be VISCOSITY instead of momentum according to Hinton
        # this implementation is so elegant that we can add any extra variables for optimization in this function,
        # without modifying anywhere else of the code, commented here for celebration, cheers.
        # classical momentum
        # cannot write like
        # up[v] = m*v - lr*g
        # up[x] = param + up[v]
        # because Theano won't update up[v] until the next function call
        new_v = self.tensor_mom*velocity - self.tensor_lr*grad
        new_p = param + new_v
        # update
        updates[velocity] = new_v
        updates[param] = new_p

        # can work as well
        # velocity = moment * velocity + learning_rate * grad
        # param = param - velocity

    def update_learning_params(self, cur_iter, max_iter):
        self.tensor_lr.set_value(self.adapt_lr.get_val(cur_iter, max_iter))
        self.tensor_mom.set_value(self.adapt_mom.get_val(cur_iter, max_iter))

    def set_optimizer_param(self, snapshot):
        if snapshot is None or snapshot.lower() == 'init':
            for velocity in self.velocity_lst:
                value = velocity.get_value(borrow=True)
                velocity.set_value(np.zeros(value.shape, dtype=value.dtype))
