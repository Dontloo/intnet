from .base import Optimizer
import theano as the
import numpy as np
from collections import OrderedDict
import theano.tensor as ten


class MyAdaOptimizer(Optimizer):
    """
    moentum + adagrad, without decay in G
    """
    def __init__(self, adapt_lr, adapt_mom, init_accum=0.01):
        Optimizer.__init__(self)
        self.adapt_lr = adapt_lr
        self.tensor_lr = the.shared(np.float32(0))
        self.adapt_mom = adapt_mom
        self.tensor_mom = the.shared(np.float32(0))

        self.init_accum = init_accum
        # optimizer parameters
        self.velocity_lst = []
        self.accum_lst = []

    def param_updates(self, param_lst):
        updates = OrderedDict()
        for p in param_lst:
            grad = the.grad(self.loss, p, disconnected_inputs='warn')
            value = p.get_value(borrow=True)

            velocity = the.shared(np.zeros(value.shape, dtype=value.dtype), broadcastable=p.broadcastable)
            accum = the.shared(self.init_accum*np.ones(value.shape, dtype=value.dtype), broadcastable=p.broadcastable)
            self.velocity_lst.append(velocity)
            self.accum_lst.append(accum)

            self.update_a_param(p, velocity, accum, grad, updates)
        return updates

    def update_a_param(self, param, velocity, accum, grad, updates):
        new_a = accum + grad**2
        new_v = self.tensor_mom*velocity + grad
        new_p = param - self.tensor_lr*new_v*(1/(ten.sqrt(new_a)))
        # update
        updates[param] = new_p
        updates[accum] = new_a
        updates[velocity] = new_v

        # new_a = accum + grad**2
        # new_v = self.moment*velocity - self.learning_rate*grad*(1/ten.sqrt(new_a))
        # updates[param] = param + new_v
        # updates[accum] = new_a
        # updates[velocity] = new_v

        # # adagrad nesterov
        # updates[param] = param - learning_rate*grad*(1/ten.sqrt(a)) + moment*v

    def update_learning_params(self, cur_iter, max_iter):
        self.tensor_lr.set_value(self.adapt_lr.get_val(cur_iter, max_iter))
        self.tensor_mom.set_value(self.adapt_mom.get_val(cur_iter, max_iter))

    def set_optimizer_param(self, snapshot):
        if snapshot is None or snapshot.lower() == 'init':
            for velocity in self.velocity_lst:
                value = velocity.get_value(borrow=True)
                velocity.set_value(np.zeros(value.shape, dtype=value.dtype))
            for accum in self.accum_lst:
                value = accum.get_value(borrow=True)
                accum.set_value(self.init_accum*np.ones(value.shape, dtype=value.dtype))
