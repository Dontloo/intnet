import theano.tensor as ten


def get_nonlinearity(arg_str):
    if arg_str is None:
        return lambda x: x
    if arg_str.lower() == "relu":
        return max0
    elif arg_str.lower() == "softmax":
        return softmax
    elif arg_str.lower() == "tanh":
        return tanh
    elif arg_str.lower() == "sigmoid":
        return sigmoid
    else:
        raise ValueError("no corresponding non-linearity.")


def softmax(x):
    return ten.nnet.softmax(x)


def sigmoid(x):
    return ten.nnet.sigmoid(x)


def tanh(x):
    return ten.tanh(x)


def max0(x):
    # The following is faster than T.maximum(0, x),
    # and it works with non-symbolic inputs as well.
    # see Lasagne https://github.com/Lasagne/Lasagne
    # Also see: https://github.com/Lasagne/Lasagne/pull/163#issuecomment-81765117
    # return 0.5 * (x + abs(x))  # could not ensure a non-negative value
    return ten.maximum(0, x)