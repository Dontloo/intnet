import operator
import functools
import time


def mul_reduce(arg_iter):
    return functools.reduce(operator.mul, arg_iter)


def calc_len(arg):
    try:
        return len(arg)
    except TypeError:
        return 1


def timeit(method):

    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()

        print '%r (%r, %r) %2.2f sec' % \
              (method.__name__, args, kw, te-ts)
        return result

    return timed
