import theano as the
import numpy as np


class Initializer:
    """
    initialization should be associated with the training function, (i.e. should not be stand-alone
    because different training objectives have different set of parameters.
    """
    def __init__(self, method, *method_args):
        self.dtype = the.config.floatX
        self.method_args = method_args
        if method.lower() == "glorot_heuristic":
            self.method = glorot_heuristic
        else:
            raise ValueError("no corresponding initializer.")

    def apply2layer(self, layer, *args):
        return self.method(layer, self.dtype, *self.method_args)


def glorot_heuristic(layer, arg_type, param_names):
    print "initializing:", layer.name, layer.__class__.__name__, layer.out_shape
    if not layer.has_param:
        return
    for key, param in layer.params.iteritems():
        if key in param_names:
            # heuristics from http://deeplearning.net/tutorial/lenet.html
            # if w is 4d, then np.prod(shape[:2]) gives the number of elements of each kernel
            # if w is 3d, then np.prod(shape[:2]) equals 1
            shp = param.get_value().shape
            bound = np.sqrt(6. / (np.sum(shp[:2]) * np.prod(shp[2:])))
            param.set_value(np.random.uniform(-bound, bound, shp).astype(arg_type))