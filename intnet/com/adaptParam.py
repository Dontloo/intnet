class AdaptParam:
    # adaptive parameter
    def __init__(self, strategy, *args):
        self.args = args
        if strategy.lower() == "fixed":
            self.method = self.fixed
        elif strategy.lower() == "linear":
            self.method = self.linear
        elif strategy.lower() == "step":
            self.method = self.step
        elif strategy.lower() == "cubic":
            self.method = self.cubic
        else:
            raise ValueError("invalid parameter strategy")

    def get_val(self, cur_iter, max_iter):
        return self.method(cur_iter, max_iter, *self.args)

    def fixed(self, cur_iter, max_iter, val):
        return val

    def linear(self, cur_iter, max_iter, start, end):
        return start+(end-start)*float(cur_iter)/max_iter

    def cubic(self, cur_iter, max_iter, start, end):
        # (0.1)^(1/3) = 0.464
        # a^3*b^3 = (a*b)^3
        # decays to its 1/8 after every half
        # http://www.wolframalpha.com/input/?i=1-%281-x%29%5E3
        return start+(end-start)*(1-(1-float(cur_iter)/max_iter)**3)

    def step(self, cur_iter, max_iter, step_list):
        """
        :param step_list: in the form of [(1e-2, 0.6),(1e-3, 1)]
        :return:
        """
        i = 0
        while float(cur_iter) / max_iter > step_list[i][1]:
            i += 1
        return step_list[i][0]
