import numpy as np


class FakeData:
    """
    fake data
    """
    def __init__(self, sze):
        self.size = sze
        self.x = np.asarray(range(sze))

    def load_selected(self, indices):
        """
        :return: a list
        """
        return self.x[indices],

    def load_batch(self, start, end):
        """
        :return: a list
        """
        return self.x[start:end],
