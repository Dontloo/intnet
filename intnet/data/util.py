import random
import intnet
import numpy as np


def random_choice(x, y, num):
    """
    randomly choose a number of data-label pairs
    """
    idx = np.random.randint(0, x.shape[0], num)
    return x[idx], y[idx]


def train_test_split(lst, cut):
    # shuffle
    random.shuffle(lst)
    # split
    if cut < 1:
        cut = int(cut * len(lst) + 0.5)
    train_lst = lst[:cut]
    test_lst = lst[cut:]
    return train_lst, test_lst


def train_test_split_per_cate(file_lst, cate_num, arg_cut):
    # read list into map
    dic = {}
    for i in range(len(file_lst)):
        _, cate_id = file_lst[i]
        if cate_id not in dic:
            dic[cate_id] = []
        dic.get(cate_id).append(file_lst[i])

    train_lst = []
    test_lst = []
    for i in range(cate_num):
        lst = dic.get(i)
        # shuffle
        random.shuffle(lst)
        # split
        if arg_cut < 1:
            cut = int(arg_cut * len(lst) + 0.5)
        else:
            raise ValueError("arg_cut must be between 0 and 1")
        train_lst.extend(lst[:cut])
        test_lst.extend(lst[cut:])
    return train_lst, test_lst


def read_caffe_lst(list_path):
    ret = []
    with open(list_path, 'rb') as f:
        for line in f:
            im_path, lab = line.split(" ")
            ret.append((im_path, int(lab)))
    return ret


def get_feature(ds, feature_fn, batch_sze=100):
    features = []
    labels = []
    for i, x, y in intnet.data.EpochBatchLauncher(ds, batch_sze):
        features.append(feature_fn(x))
        labels.append(y)
        # subtract mean
    return np.vstack(features), np.hstack(labels)