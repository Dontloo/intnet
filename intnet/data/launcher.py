import math
from .util import *


# --------------------------------- cate/bin/pair/fake ---------------------------------
class RandomBatchLauncher:
    """
    deprecated
    launch batches randomly from given data source
    usage:
        for i,x,y in RandomBatchLauncher(steps, size):
            strategy(i,x,y)
    can support flexible training functions (e.g. use a larger momentum at the beginning and a smaller one at the end
    """

    def __init__(self, ds, batch_sze, steps, replace=False):
        self.steps = steps
        self.batch_sze = batch_sze
        self.ds = ds
        self.size = ds.size
        self.replace = replace

    def __iter__(self):
        self.i = 0
        return self

    def next(self):
        if self.i < self.steps:
            self.i += 1
            return (self.i - 1,) + self.ds.load_selected(
                np.random.choice(self.size, self.batch_sze, replace=self.replace))
        else:
            raise StopIteration()


class EpochBatchLauncher:
    """
    launch all data in split batches
    """

    def __init__(self, ds, batch_sze, steps=None, shuffle=False, index_subset=None):
        self.batch_sze = batch_sze
        self.ds = ds
        self.size = self.ds.size
        self.shuffle = shuffle
        self.epoch_steps = int(math.ceil(self.size / float(batch_sze)))  # steps needed to go over the data set once
        self.steps = steps if steps is not None else self.epoch_steps
        # index subset for cross-validation
        self.index_subset = index_subset if index_subset is not None else np.asarray(range(self.size))

    def __iter__(self):
        self.i = 0
        return self

    def next(self):
        if self.i < self.steps:
            j = self.i % self.epoch_steps

            if self.shuffle and j == 0:
                print "shuffling.."
                self.index_subset = np.random.permutation(self.index_subset)  # shuffle after an epoch

            batch = self.ds.load_selected(self.index_subset[j * self.batch_sze:(j + 1) * self.batch_sze])
            self.i += 1
            return (self.i - 1,) + batch
        else:
            raise StopIteration()


# --------------------------------- cate/bin/pair/fake ---------------------------------


# --------------------------------- cate only ---------------------------------
class RandomPairLauncher:
    """
    launch batches in pairs mainly for the purpose of verification signals
    at present the proportion of non-overlapping pairs of the same/different identity
    is fixed to (approximately) 0.5 for the sake of balanced data
    """

    def __init__(self, ds, batch_sze, steps, cate_smooth=0, stgy='', ds_pro=1):
        """
        :param cate_smooth:
            by default the triplets of an identity will be sampled according to the number of samples it has,
            in order to make full use of the training set.
            we can add a smoothing term to compensate the unbalanced data set,
            if cate_smooth is infinity or "uniform", identity triplets will be sampled by a uniform distribution
        """
        self.steps = steps
        self.stgy = stgy
        if batch_sze % 4 != 0:
            raise ValueError("batch size must be a multiple of 4")
        self.pair_num = batch_sze / 2
        self.ds = ds
        self.size = ds.size
        self.idx_map = ds.id_map()
        self.same_idx = []
        self.random_idx = []
        self.offset = 0
        if ds_pro < 1:
            self.idx_map = dict(
                (sorted(self.idx_map.iteritems(), key=lambda x: len(x[1]), reverse=True))[:int(ds.size * ds_pro)])

        if cate_smooth == float("inf") or cate_smooth == "uniform":
            self.p = None
        else:
            self.p = np.asarray([len(self.idx_map[key]) + cate_smooth for key in self.idx_map.keys()], dtype=np.float64)
            self.p /= np.sum(self.p)

    def __iter__(self):
        self.i = 0
        self.offset = 0
        return self

    def next(self):
        if self.i < self.steps:
            self.i += 1
            if self.stgy == 'whole':
                self.offset += self.pair_num
                if self.offset > len(self.same_idx) or self.offset > len(self.random_idx):
                    print "shuffling..."
                    self.same_idx, self.random_idx = self.random_pair_whole(0.5)
                    self.offset = self.pair_num

                x_same, y_same = self.ds.load_selected(self.same_idx[self.offset - self.pair_num: self.offset])
                x_rand, y_rand = self.ds.load_selected(self.random_idx[self.offset - self.pair_num: self.offset])
                x = np.concatenate((x_same, x_rand), axis=0)
                y = np.concatenate((y_same, y_rand), axis=0)

                return self.i - 1, x, y
            else:
                return (self.i - 1,) + self.random_pair_batch(self.pair_num, 0.5)
        else:
            raise StopIteration()

    def random_pair_batch(self, pair_num, same_pro):
        """
        for sampling pairs, first half are positive pairs, the second half are random pairs.
        """
        # same pairs
        id_lst = np.random.choice(self.idx_map.keys(), int(pair_num * same_pro), p=self.p, replace=False)
        sample_idx = []
        for i in id_lst:
            sample_idx.extend(list(np.random.choice(self.idx_map[i], 2, replace=False)))

        x_same, y_same = self.ds.load_selected(sample_idx)

        # random (different) pairs
        num_remain = pair_num - int(pair_num * same_pro)
        x_rand, y_rand = self.ds.load_selected(np.random.choice(self.size, num_remain * 2, replace=False))
        x = np.concatenate((x_same, x_rand), axis=0)
        y = np.concatenate((y_same, y_rand), axis=0)
        return x, y

    def random_pair_whole(self, same_pro):
        """
        for sampling pairs on whole data set
        """

        same_idx = []
        random_idx = []
        id_lst = self.idx_map.keys()
        # np.random.shuffle(id_lst)
        for id in id_lst:
            num = int(len(self.idx_map[id]) * same_pro)
            if num % 2 != 0:
                num += (1 if random.random() < 0.5 else -1)

            one = self.idx_map[id]
            random.shuffle(one)

            i = 0
            while i < num:
                same_idx.append(one[i:i + 2])
                i += 2
            random_idx.extend(one[num:])

        random.shuffle(same_idx)
        same_idx = [val for sublist in same_idx for val in sublist]
        random.shuffle(random_idx)

        return same_idx, random_idx


class RandomTripletLauncher:
    """
    launch batches in triplets for the purpose of verification signals
    (more efficient, flexible and sensible than the RandomPairLauncher version
    """

    def __init__(self, ds, batch_sze, steps, cate_smooth=0, ds_pro=1):
        """
        :param cate_smooth:
            by default the triplets of an identity will be sampled according to the number of samples it has,
            in order to make full use of the training set.
            we can add a smoothing term to compensate the unbalanced data set,
            if cate_smooth is infinity or "uniform", identity triplets will be sampled by a uniform distribution
        """
        self.steps = steps
        if batch_sze % 3 != 0:
            raise ValueError("batch size muse be a multiple of 3")
        self.triplet_num = batch_sze / 3
        self.ds = ds
        # select subset of categories with the most amount of samples
        self.idx_map = dict((sorted(self.ds.id_map().iteritems(), key=lambda x: len(x[1]), reverse=True))[:int(ds.size*ds_pro)]) if ds_pro < 1 else ds.id_map()

        if cate_smooth == float("inf") or cate_smooth == "uniform":
            self.p = None
        else:
            self.p = np.asarray([len(self.idx_map[key]) + cate_smooth for key in self.idx_map.keys()], dtype=np.float64)
            self.p /= np.sum(self.p)

    def __iter__(self):
        self.i = 0
        self.offset = 0
        return self

    def next(self):
        if self.i < self.steps:
            self.i += 1
            return (self.i - 1,) + self.random_triplet_batch(self.triplet_num)
        else:
            raise StopIteration()

    def random_triplet_batch(self, triplet_num):
        """
        for sampling triplets, in the form of x, positive, negative.
        """
        # generate indices
        id_lst = np.random.choice(self.idx_map.keys(), 2 * triplet_num, p=self.p, replace=False)
        sample_idx = []
        for i in range(triplet_num):
            x, p = np.random.choice(self.idx_map[id_lst[2 * i]], 2, replace=False)
            n = np.random.choice(self.idx_map[id_lst[2 * i + 1]])
            sample_idx.append(x)
            sample_idx.append(p)
            sample_idx.append(n)
        # launch data
        x, y = self.ds.load_selected(sample_idx)
        return x, y


class EpochTripletLauncher:
    """
    launch batches in triplets for the purpose of verification signals
    (more efficient, flexible and sensible than the RandomPairLauncher version
    """

    def __init__(self, ds, batch_sze, steps, ds_pro=1):
        """
        :param cate_smooth:
            by default the triplets of an identity will be sampled according to the number of samples it has,
            in order to make full use of the training set.
            we can add a smoothing term to compensate the unbalanced data set,
            if cate_smooth is infinity or "uniform", identity triplets will be sampled by a uniform distribution
        """
        self.steps = steps
        if batch_sze % 3 != 0:
            raise ValueError("batch size muse be a multiple of 3")
        self.triplet_num = batch_sze / 3
        self.ds = ds
        # select subset of categories with the most amount of samples
        self.idx_map = dict((sorted(self.ds.id_map().iteritems(), key=lambda x: len(x[1]), reverse=True))[:int(ds.size*ds_pro)]) if ds_pro < 1 else ds.id_map()
        # index subset for category subset
        self.index_subset = np.concatenate(self.idx_map.values())

        self.size = len(self.index_subset)
        self.epoch_steps = int(math.ceil(self.size / float(batch_sze)))  # steps needed to go over the data set once
        self.steps = steps if steps is not None else self.epoch_steps

    def __iter__(self):
        self.i = 0
        self.offset = 0
        return self

    def next(self):
        if self.i < self.steps:
            j = self.i % self.epoch_steps
            if j == 0:
                print "shuffling.."
                self.index_subset = np.random.permutation(self.index_subset)  # shuffle after an epoch

            anchor_idx = self.index_subset[j * self.triplet_num:(j + 1) * self.triplet_num]
            _, anchor_y = self.ds.load_selected(anchor_idx)
            batch = self.random_triplet_batch(anchor_idx, anchor_y)
            self.i += 1
            return (self.i - 1,) + batch
        else:
            raise StopIteration()

    def random_triplet_batch(self, idx, y):
        """
        for sampling triplets on whole data set
        """
        sample_idx = []
        for i in range(len(idx)):
            p_idx = np.random.choice(set(self.idx_map[y[i]])-{idx[i]})
            n_idx = np.random.choice(set(self.index_subset)-set(self.idx_map[y[i]]))
            sample_idx.append(idx[i])
            sample_idx.append(p_idx)
            sample_idx.append(n_idx)
        # launch data
        x, y = self.ds.load_selected(sample_idx)
        return x, y


class CategoryLauncher:
    """
    every iteration returns all x,y that from the same category
    """

    def __init__(self, ds):
        self.ds = ds
        self.idx_map = ds.id_map()
        self.it = self.idx_map.iterkeys()

    def __iter__(self):
        return self

    def next(self):
        return self.ds.load_selected(self.idx_map[self.it.next()])


# --------------------------------- cate only ---------------------------------


if __name__ == "__main__":
    from fakeData import FakeData

    data = FakeData(10)
    for i, x in RandomBatchLauncher(10, 3, data):
        print i, x
