from PIL import Image
import numpy as np
import random
from prepro import get_im_pro_fn


def get_attr_by_idx(tag, idx):
    return tag[idx]


def random_launch(file_lst, batch_sze, process_data_fn=get_im_pro_fn(), process_tag_fn=lambda x:x, x_dtype="float32", y_dtype="int32"):
    """
    randomly launch a patch from file list, with a given function on how to process tags,
    input is assumed to be image paths,
    :param file_lst: items in the list is assumed to be in the form of (pic_path, tags)
    :param process_data_fn: a function to process data
    :param process_tag_fn: a function to process tags
    :return: x: data, y: labels
    """
    x_lst = []
    y_lst = []
    for i in range(batch_sze):
        # random choose
        data, tag = random.choice(file_lst)
        # vectorize
        x_lst.append(process_data_fn(data))
        y_lst.append(process_tag_fn(tag))
    x = np.asarray(x_lst, dtype=x_dtype)
    y = np.asarray(y_lst, dtype=y_dtype)
    return x, y


def launch_all(file_lst, process_data_fn=get_im_pro_fn(), process_tag_fn=lambda x:x, x_dtype="float32", y_dtype="int32"):
    """
    return data has to be in the same order as file list
    """
    x_lst = []
    y_lst = []
    for data, tag in file_lst:
        x_lst.append(process_data_fn(data))
        y_lst.append(process_tag_fn(tag))
    x = np.asarray(x_lst, dtype=x_dtype)
    y = np.asarray(y_lst, dtype=y_dtype)
    return x, y


def launch_selected(file_lst, idx_lst, process_data_fn=get_im_pro_fn(), process_tag_fn=lambda x:x, x_dtype="float32", y_dtype="int32"):
    """
    return data has to be in the same order as file list
    """
    x_lst = []
    y_lst = []
    for idx in idx_lst:
        data, tag = file_lst[idx]
        x_lst.append(process_data_fn(data))
        y_lst.append(process_tag_fn(tag))
    x = np.asarray(x_lst, dtype=x_dtype)
    y = np.asarray(y_lst, dtype=y_dtype)
    return x, y


def launch_pairs(pair_lst, process_data_fn=get_im_pro_fn(), process_tag_fn=lambda x:x, x_dtype="float32", y_dtype="int32"):
    """
    return data has to be in the same order as file list
    """
    x1_lst = []
    x2_lst = []
    y_lst = []
    for x1, x2, y in pair_lst:
        x1_lst.append(process_data_fn(x1))
        x2_lst.append(process_data_fn(x2))
        y_lst.append(process_tag_fn(y))
    x1 = np.asarray(x1_lst, dtype=x_dtype)
    x2 = np.asarray(x2_lst, dtype=x_dtype)
    y = np.asarray(y_lst, dtype=y_dtype)
    return x1, x2, y


def launch_selected_pairs(pair_lst, idx_lst, process_data_fn=get_im_pro_fn(), process_tag_fn=lambda x:x, x_dtype="float32", y_dtype="int32"):
    """
    return data has to be in the same order as file list
    """
    x1_lst = []
    x2_lst = []
    y_lst = []
    for idx in idx_lst:
        x1, x2, y = pair_lst[idx]
        x1_lst.append(process_data_fn(x1))
        x2_lst.append(process_data_fn(x2))
        y_lst.append(process_tag_fn(y))
    x1 = np.asarray(x1_lst, dtype=x_dtype)
    x2 = np.asarray(x2_lst, dtype=x_dtype)
    y = np.asarray(y_lst, dtype=y_dtype)
    return x1, x2, y
