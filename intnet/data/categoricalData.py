from .launch import *
from collections import defaultdict


class CategoricalData:
    """
    a wrapper for categorical data

    :param file_lst: (path, cate_id)
    """

    def __init__(self, file_lst, train_storage, process_data_fn, process_tag_fn=lambda x: x, x_dtype="float32", y_dtype="int32"):
        self.x_dtype = x_dtype
        self.y_dtype = y_dtype
        self.process_data_fn = process_data_fn
        self.process_tag_fn = process_tag_fn
        self.file_lst = file_lst
        self.train_storage = train_storage  # where to keep training data
        self.size = len(self.file_lst)
        if self.train_storage == "in_memory":
            self.x, self.y = launch_all(self.file_lst, process_data_fn=self.process_data_fn, process_tag_fn=self.process_tag_fn,
                                                       x_dtype=self.x_dtype, y_dtype=self.y_dtype)

        self.idx_map = None
        self.idx_list = None

    def load_selected(self, indices):
        if self.train_storage == "in_memory":
            x = self.x[indices]
            y = self.y[indices]
        else:
            x, y = launch_selected(self.file_lst, indices, process_data_fn=self.process_data_fn, process_tag_fn=self.process_tag_fn,
                                                  x_dtype=self.x_dtype, y_dtype=self.y_dtype)
        return x, y

    def load_batch(self, start, end):
        if self.train_storage == "in_memory":
            x = self.x[start:end]
            y = self.y[start:end]
        else:
            x, y = launch_all(self.file_lst[start:end], process_data_fn=self.process_data_fn, process_tag_fn=self.process_tag_fn,
                                             x_dtype=self.x_dtype, y_dtype=self.y_dtype)
        return x, y

    def id_map(self):
        """
        :return: {id:[idx,idx...]}
        """
        if self.idx_map is not None:
            return self.idx_map

        idx_map = defaultdict(list)
        for i in range(len(self.file_lst)):
            _, cate_id = self.file_lst[i]
            idx_map[cate_id].append(i)
        # convert to numpy array
        self.idx_map = {}
        for key in idx_map:
            self.idx_map[key] = np.asarray(idx_map[key])
        return self.idx_map

    def id_list(self):
        """
        :return: [(idx,id)...]
        """
        if self.idx_list is not None:
            return self.idx_list
        self.idx_list = []
        for i in range(len(self.file_lst)):
            if self.train_storage == "in_memory":
                self.idx_list.append((i, self.y[i]))
            else:
                _, cate_id = self.file_lst[i]
                self.idx_list.append((i, cate_id))

        return self.idx_list
