from .launch import *


class BinaryData:
    """
    a wrapper for binary data
    """
    def __init__(self, file_lst, train_storage, process_tag_fn, x_dtype="float32", y_dtype="int32"):
        self.x_dtype = x_dtype
        self.y_dtype = y_dtype
        self.process_tag_fn = process_tag_fn
        self.file_lst = file_lst
        self.train_storage = train_storage  # where to keep training data
        self.size = len(self.file_lst)
        if self.train_storage == "in_memory":
            self.x, self.y = launch_all(self.file_lst, process_tag_fn=self.process_tag_fn, x_dtype=self.x_dtype, y_dtype=self.y_dtype)

    def load_selected(self, indices):
        if self.train_storage == "in_memory":
            x = self.x[indices]
            y = self.y[indices]
        else:
            x, y = launch_selected(self.file_lst, indices, process_tag_fn=self.process_tag_fn, x_dtype=self.x_dtype, y_dtype=self.y_dtype)
        return x, y

    def load_batch(self, start, end):
        if self.train_storage == "in_memory":
            x = self.x[start:end]
            y = self.y[start:end]
        else:
            x, y = launch_all(self.file_lst[start:end], process_tag_fn=self.process_tag_fn, x_dtype=self.x_dtype, y_dtype=self.y_dtype)
        return x, y
