from PIL import Image
import numpy as np
import random
from functools import partial


def one_hot(id, category_num, noise=0):
    """
    'one_hot' vectors are not preferable
    """
    y_vec = np.zeros((category_num,))
    y_vec[id] = 1
    if noise == 0:
        return y_vec
    else:
        return (1-noise)*y_vec+noise/float(category_num)


def load_im(im_file, color_space):
    im = Image.open(im_file).convert(color_space)
    return im


def random_resize(im, lo, hi):
    s = random.uniform(lo, hi)
    return im.resize(map(lambda x: int(x*s+0.5), im.size), Image.ANTIALIAS)


def vector_im(im, color_space, flatten=False, scale=False):
    if color_space == "L":  # L (8-bit pixels, black and white)
        # numpy.newaxis
        ret = np.asarray([np.asarray(im)])
    elif color_space == "RGB":  # RGB (3x8-bit pixels, true color)
        ret = np.asarray(im).transpose(2, 0, 1)
    else:
        raise ValueError("unknown color space")

    if flatten:
        ret = ret.reshape(-1,)

    if scale:
        ret = ret.astype(float)/255
    return ret


def random_crop(im, arg_w, arg_h):
    im_w, im_h = im.size
    if im_w < arg_w or im_h < arg_h:
        raise ValueError("image size is smaller than crop size.")
    w = random.randint(0, im_w-arg_w)
    h = random.randint(0, im_h-arg_h)
    return im.crop((w, h, w+arg_w, h+arg_h))


def get_im_pro_fn(color_space="RGB", resize_interval=(1, 1), crop_shape=(None, None), scale=True, flatten=False):
    # load_fn = lambda x: load_im(x, color_space)
    #
    # if crop_shape != (None, None):
    #     crop_fn = lambda x: random_crop(load_fn(x), *crop_shape)
    # else:
    #     crop_fn = load_fn
    #
    # if resize_interval != (1, 1):
    #     resize_fn = lambda x: random_resize(crop_fn(x), *resize_interval)
    # else:
    #     resize_fn = crop_fn
    #
    # vec_fn = lambda x: vector_im(resize_fn(x), color_space)
    # return vec_fn

    if crop_shape == (None, None) and resize_interval == (1, 1):
        return lambda x:vector_im(load_im(x, color_space), color_space, flatten, scale)
    if resize_interval == (1, 1):
        return lambda x:vector_im(random_crop(load_im(x, color_space), *crop_shape), color_space, flatten, scale)
    if crop_shape == (None, None):
        return lambda x:vector_im(random_resize(load_im(x, color_space), *resize_interval), color_space, flatten, scale)
    return lambda x:vector_im(random_crop(random_resize(load_im(x, color_space), *resize_interval), *crop_shape), color_space, flatten, scale)


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    # fn = get_im_pro_fn(color_space="RGB", resize_interval=(0.5, 0.8), crop_shape=(400, 300))
    # im = Image.fromarray(fn("/home/guest/Desktop/dev/git/intnet/intnet/models/curves0.png").transpose(1, 2, 0), 'RGB')
    fn = get_im_pro_fn(color_space="L", resize_interval=(1., 1.), scale=True)  # load image as grayscale
    print fn("/home/guest/Desktop/dev/data/mnist/train/0/246.jpg")[0]
    plt.imshow(fn("/home/guest/Desktop/dev/data/mnist/train/0/246.jpg")[0], cmap='Greys_r')
    plt.show()
