class XVali:
    def __init__(self, lst, num_folds):
        self.it = range(num_folds).__iter__()
        self.lst = lst
        self.num_folds = num_folds

    def __iter__(self):
        return self

    def next(self):
        i = self.it.next()
        return [x for j, x in enumerate(self.lst) if j%self.num_folds != i],\
            self.lst[i::self.num_folds]

if __name__ == "__main__":
    import pickle
    file_path = "../../../../data/WebFaceNorm/files/ac1024_37*37_gray.pickle"

    with open(file_path, 'rb') as f:
        row, column, channel, cate_num = pickle.load(f)
        data_lst = pickle.load(f)

    for train, test in XVali(data_lst, 5):
        print len(train), len(test)

    print len(data_lst)
