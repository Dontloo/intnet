from .launch import *


class PairMatchData:
    """
    a wrapper for pair data
    data source for matching pairs
    """
    def __init__(self, arg_file_lst, arg_train_storage, process_data_fn, process_tag_fn=lambda x:x, x_dtype="float32", y_dtype="int32"):
        self.process_data_fn = process_data_fn
        self.process_tag_fn = process_tag_fn
        self.x_dtype = x_dtype
        self.y_dtype = y_dtype
        self.pair_lst = arg_file_lst
        self.train_storage = arg_train_storage  # where to keep training data
        self.size = len(self.pair_lst)

    def load_all(self):
        return launch_pairs(self.pair_lst, process_data_fn=self.process_data_fn, process_tag_fn=self.process_tag_fn, x_dtype=self.x_dtype, y_dtype=self.y_dtype)

    def load_batch(self, start, end):
        return launch_pairs(self.pair_lst[start:end], process_data_fn=self.process_data_fn, process_tag_fn=self.process_tag_fn, x_dtype=self.x_dtype, y_dtype=self.y_dtype)

    def load_selected(self, indices):
        return launch_selected_pairs(self.pair_lst, indices, process_data_fn=self.process_data_fn, process_tag_fn=self.process_tag_fn, x_dtype=self.x_dtype, y_dtype=self.y_dtype)
