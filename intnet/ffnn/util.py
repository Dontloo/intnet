import collections
import intnet
import theano as the
import theano.tensor as ten

# ----------------- map-reducers -----------------
def dfs_layer_traverse(root, func, arg):
    """
    preform depth first search starting from the 'root' layer.
    map a function to each layer, in the form of func(vertex, arg)

    N.B. func is executed after all children have been visited
    """
    visited = set()

    def dfs(vertex):
        if vertex in visited:
            return
        else:
            visited.add(vertex)
        # recur
        if isinstance(vertex.pred, intnet.layer.Layer):
            dfs(vertex.pred)
        elif isinstance(vertex.pred, collections.Iterable):
            for pred in vertex.pred:
                dfs(pred)
        else:
            pass
        # execute after all children have been visited
        func(vertex, arg)

    dfs(root)
    return arg
# ----------------- map-reducers -----------------


def assign_computation(layer, arg, train):
    # different computation graphs for training and test
    print "building graph", layer.name, layer.__class__.__name__
    layer.out_var = layer.get_out(train)


def collect_inputs(arg):
    if isinstance(arg, intnet.layer.Layer):
        # should cast to a list if only one input, in correspondence with theano
        return [arg.in_var]
    return map(lambda x: x.in_var, arg)


def collect_outputs(arg):
    if isinstance(arg, intnet.layer.Layer):
        return arg.out_var
    if isinstance(arg, ten.TensorVariable) or isinstance(arg, the.compile.sharedvalue.SharedVariable) or isinstance(arg, the.sandbox.cuda.var.CudaNdarrayVariable):
        return arg
    # arg is a list
    lst = []
    for lay in arg:
        if isinstance(lay, intnet.layer.Layer):
            lst.append(lay.out_var)
        elif isinstance(lay, ten.TensorVariable) or isinstance(lay, the.compile.sharedvalue.SharedVariable) or isinstance(lay, the.sandbox.cuda.var.CudaNdarrayVariable):
            lst.append(lay)
        else:
            print lay.__class__.__name__
            pass  # can't handle
    return lst
