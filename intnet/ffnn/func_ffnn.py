from intnet.layer import Layer
from intnet.com import *
import matplotlib.pyplot as plt
import pickle
from .util import *

'''
the traversal method (dfs in this case) should be associated with the network instead of the optimizer or initializer,
because different types of networks (ffnn/rnn) can have different ways of traversal.
'''


class FFNNOptimize:
    def __init__(self, loss_layer):
        """
        only have to keep track of the topmost/output layer of the network
        because FFNN is essentially a DAG, knowing a sink vertex is enough.

        when instantiating functions, first the network is traversed,
        for each layer,
        the corresponding part of the computation graph is connected after all its children have been visited
        """
        self.loss_layer = loss_layer
        self.results = []
        # check duplicate layer names
        self.check_layer_name()
        # assign a unique name for each unnamed layer
        self.assign_name()
        self.print_name()
        # build computation graph
        self.build_graph(self.loss_layer)

    # ----------------- build functions -----------------
    @staticmethod
    def build_graph(loss_layer):
        # re-build the computation graph
        dfs_layer_traverse(loss_layer, lambda x, y: assign_computation(x, y, train=True), None)

    def opt_fn(self, in_layers, out_layers, opt):
        """
        this function must be called after the loss is set,
        since we may want to output the gradient terms (wrt the loss) as well
        """
        # collect inputs
        inputs = collect_inputs(in_layers)
        # collect outputs
        outputs = collect_outputs(out_layers)
        # collect updates
        updates = self.collect_updates(opt)

        self.results = [[] for _ in range(calc_len(outputs))]
        # return a decorated function that saves the results
        return self.res_dec(the.function(inputs, outputs, updates=updates, on_unused_input='warn'))

    def collect_updates(self, opt):
        # optimization updates
        opt.set_loss(self.loss_layer.out_var)

        # collect parameters for backpropagation
        def collect_param_backprop(layer, arg):
            if layer.has_param:
                for key, param in layer.params.iteritems():
                    if key in ["w", "b", "g"]:
                        arg.append(param)

        # collect parameters for non-gradient-based updates
        def collect_param_non_grad(layer, arg_updates):
            if layer.non_grad_updates:
                for key, val in layer.updates:
                    arg_updates[key] = val

        params = dfs_layer_traverse(self.loss_layer, collect_param_backprop, [])
        updates = opt.param_updates(params)
        # non-gradient updates do not depend on the optimizer
        updates = dfs_layer_traverse(self.loss_layer, collect_param_non_grad, updates)
        return updates

    def res_dec(self, func):
        """
        decorator function to save results
        unpack the output when necessary
        """
        def res_wrapper(*args):
            res = func(*args)
            if isinstance(res, np.ndarray):
                self.results[0].append(res)
            else:
                for i in range(len(res)):
                    self.results[i].append(res[i])
            return res
        return res_wrapper
    # ----------------- build function -----------------

    # ----------------- layer name check/assign -----------------
    def check_layer_name(self):
        def name_append(layer, lst):
            if layer.name is not None:
                lst.append(layer.name)
        name_list = dfs_layer_traverse(self.loss_layer, name_append, [])
        if len(name_list) != len(set(name_list)):
            raise ValueError("duplicate layer names.")

    def assign_name(self):
        def name_assign(layer, name):
            if layer.name is None:
                layer.name = str(name[0])
                name[0] += 1
        dfs_layer_traverse(self.loss_layer, name_assign, [0])

    def print_name(self):
        def name_print(layer, arg):
            arg.append(layer.name)
        print dfs_layer_traverse(self.loss_layer, name_print, [])
    # ----------------- layer name check/assign -----------------

    # ----------------- parameter save/load/initialize -----------------
    # parameters are organized into maps, in the form of layer:key:value,
    # for the convenience of being loaded into a different network.
    def init_param(self, arg="default"):
        self.default_init = intnet.com.Initializer("glorot_heuristic", ["w", "g"])
        if arg is None or arg.lower() == "glorot_heuristic" or arg.lower() == "default":
            dfs_layer_traverse(self.loss_layer, self.default_init.apply2layer, [])
        else:
            f = open(arg, 'rb')
            param_map = pickle.load(f)
            dfs_layer_traverse(self.loss_layer, self.load_layer_param, param_map)
            f.close()

    def save_param(self, file_name):
        f = open(file_name, 'wb')
        param_map = dict()
        dfs_layer_traverse(self.loss_layer, self.save_layer_param, param_map)
        pickle.dump(param_map, f)
        f.close()

    def save_layer_param(self, layer, param_map):
        print "saving:", layer.name, layer.__class__.__name__, layer.out_shape
        if not layer.has_param:
            return
        tmp = dict()
        for key, param in layer.params.iteritems():
            tmp[key] = param.get_value()
        param_map[layer.name] = tmp

    def load_layer_param(self, layer, param_map):
        if not layer.has_param:
            return
        print "loading:", layer.name, layer.__class__.__name__, layer.out_shape
        if layer.name not in param_map:
            print "warning: parameters of layer", layer.name, "not provided, using default initialization."
            self.default_init.apply2layer(layer)
        else:
            for key, param in layer.params.iteritems():
                layer.params[key].set_value(param_map[layer.name][key])
    # ----------------- parameter save/load/initialize -----------------

    # ----------------- network functions -----------------
    def save_res(self, func):
        def wrapper(*args):
            res = func(*args)
            self.results.append(res)
            return res
        return wrapper

    def plot(self, title_lst, name):
        """
        plot the results in subplots and save to drive
        """
        plt.figure(1)
        for i in range(len(title_lst)):
            plt.subplot(len(title_lst), 1, i+1)
            plt.plot(self.results[i])
            plt.title(title_lst[i])
        # save has to be in front of show
        plt.savefig(name)
        # plt.show()
    # ----------------- network functions -----------------


def ffnn_fn(in_layers, out_layers):
    # re-build the computation graph
    if isinstance(out_layers, Layer):
        root_layer = out_layers
    else:
        root_layer = out_layers[0]
    dfs_layer_traverse(root_layer, lambda x, y: assign_computation(x, y, train=False), None)
    # collect inputs
    inputs = collect_inputs(in_layers)
    # collect outputs
    outputs = collect_outputs(out_layers)
    return the.function(inputs, outputs)